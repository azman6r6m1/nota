DOMAIN_NAME          = 'dom01'
DOMAIN_HOME          = '/app/oracle/ohs/domains/dom01'
JAVA_HOME            = '/app/oracle/ohs/jdk'
TEMPLATE_NAME        = 'Oracle HTTP Server (Standalone)'
NODEMANAGER_HOSTNAME = 'localhost'
NODEMANAGER_PORT     = '20001'
NODEMANAGER_PASSWORD = 'abcd1234'
MACHINE_NAME         = 'localmachine'

selectTemplate(TEMPLATE_NAME)
loadTemplates()
setOption('DomainName', DOMAIN_NAME)
setOption('JavaHome', JAVA_HOME)
setOption('NodeManagerType', 'PerDomainNodeManager')
setOption('ServerStartMode', 'prod')

cd('/')
create(DOMAIN_NAME, 'SecurityConfiguration')
cd('/SecurityConfiguration/' + DOMAIN_NAME)
set('NodeManagerUsername', 'nodemanager')
set('NodeManagerPasswordEncrypted', NODEMANAGER_PASSWORD)
cd('/Machine/' + MACHINE_NAME)
create(MACHINE_NAME, 'NodeManager')
cd('NodeManager/' + MACHINE_NAME)
set('ListenAddress', NODEMANAGER_HOSTNAME)
set('ListenPort', int(NODEMANAGER_PORT))
set('NMType', 'Plain')
set('UserName', 'nodemanager')
set('PasswordEncrypted', NODEMANAGER_PASSWORD)

cd('/')
delete('ohs1', 'SystemComponent')
create('ohs01', 'SystemComponent')

writeDomain(DOMAIN_HOME)
closeTemplate()
