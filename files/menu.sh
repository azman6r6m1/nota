#!/bin/bash

SELECTED_DOMAIN=
SELECTED_SERVER_NAME=
SELECTED_SERVER_TYPE=
SELECTED_TASK=

f_header()
{
  echo '*******************************'
  echo '* ORACLE MIDDLEWARE MENU v0.1 *'
  echo '*******************************'
  echo ''
}

f_check_exit()
{
  if [ "$1" == "x" ]
  then
    echo 'Exiting...'
    exit 0
  fi
}

f_select_domain()
{
  echo 'Select domain...'
  echo ''
  declare -a DOMAINS_LIST
  DOMAIN_INDEX=1
  for DOMAIN in $(ls "${DOMAINS_DIR}")
  do
    DOMAINS_LIST[DOMAIN_INDEX]="${DOMAIN}"
    printf "%-2s %s\n" "${DOMAIN_INDEX}." "${DOMAIN}"
    ((DOMAIN_INDEX=DOMAIN_INDEX+1))
  done
  echo ''
  read -p 'Enter selection ("x" to exit): ' SELECTED_INDEX
  f_check_exit "${SELECTED_INDEX}"
  SELECTED_DOMAIN="${DOMAINS_LIST[${SELECTED_INDEX}]}"
}

f_select_server()
{
  echo 'Select server...'
  echo ''
  
  CONFIG_FILE="${DOMAINS_DIR}/${SELECTED_DOMAIN}/config/config.xml"

  declare -a SERVER_NAMES_LIST
  declare -a SERVER_TYPES_LIST

  SERVER_INDEX=1

  while read LINE
  do
  if [ "${LINE}" == '<server>' ] || [ "${LINE}" == '<system-component>' ]
  then
    SERVER_TYPES_LIST[SERVER_INDEX]="WebLogic"
  elif [ "${LINE}" == '</server>' ] || [ "${LINE}" == '</system-component>' ]
  then
    printf "%-2s %s\n" "${SERVER_INDEX}." "${SERVER_NAMES_LIST[${SERVER_INDEX}]}  [${SERVER_TYPES_LIST[${SERVER_INDEX}]}]"
    ((SERVER_INDEX=SERVER_INDEX+1))
  elif [ "${LINE:0:6}" == '<name>' ]
  then
    SERVER_NAMES_LIST[SERVER_INDEX]="$(echo "${LINE}" | awk -F'>' '{print $2}' | awk -F'<' '{print $1}')"
  elif [ "${LINE:0:16}" == '<component-type>' ]
  then
    SERVER_TYPES_LIST[SERVER_INDEX]="$(echo "${LINE}" | awk -F'>' '{print $2}' | awk -F'<' '{print $1}')"
  fi
  done <<< $(sed -n -e '/<server>/,/<\/server>/p' -e'/<system-component>/,/<\/system-component>/p' "${CONFIG_FILE}" | grep -P '</?(server|system-component|name|component-type)>')
  
  echo ''
  read -p 'Enter selection ("x" to exit): ' SELECTED_INDEX
  f_check_exit "${SELECTED_INDEX}"
  SELECTED_SERVER_NAME="${SERVER_NAMES_LIST[${SELECTED_INDEX}]}"
  SELECTED_SERVER_TYPE="${SERVER_TYPES_LIST[${SELECTED_INDEX}]}"
}

f_select_task_weblogic()
{
  echo 'Select task...'
  echo ''
  
  declare -a WEBLOGIC_TASKS_LIST
  WEBLOGIC_TASKS_LIST[1]='Check status'
  WEBLOGIC_TASKS_LIST[2]='Start server'
  WEBLOGIC_TASKS_LIST[3]='Stop server'
  WEBLOGIC_TASKS_LIST[4]='View server log'
  WEBLOGIC_TASKS_LIST[5]='View server STDOUT'
  WEBLOGIC_TASKS_LIST[6]='Deploy application(s)'
  
  for TASK_INDEX in "${!WEBLOGIC_TASKS_LIST[@]}"
  do
    printf "%-2s %s\n" "${TASK_INDEX}." "${WEBLOGIC_TASKS_LIST[${TASK_INDEX}]}"
  done
  echo ''
  read -p 'Enter selection ("x" to exit): ' SELECTED_INDEX
  f_check_exit "${SELECTED_INDEX}"
  SELECTED_TASK="${WEBLOGIC_TASKS_LIST[${SELECTED_INDEX}]}"
}

while [ "${SELECTED_DOMAIN}" == "" ]
do
  clear
  f_header
  f_select_domain
done

while [ "${SELECTED_SERVER_NAME}" == "" ]
do
  clear
  f_header
  echo "Domain name = ${SELECTED_DOMAIN}"
  echo ''
  f_select_server
done

if [ "${SELECTED_SERVER_TYPE}" == "WebLogic" ]
then
  while [ "${SELECTED_TASK}" == "" ]
  do
    clear
    f_header
    echo "Domain name = ${SELECTED_DOMAIN}"
    echo "Server name = ${SELECTED_SERVER_NAME}"
    echo "Server type = ${SELECTED_SERVER_TYPE}"
    echo ''
    f_select_task_weblogic
  done
fi

echo "${SELECTED_TASK}"