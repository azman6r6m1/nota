# By Azman Kudus
# Last update on 17 Jan 2022

from datetime import datetime
import os
import re
import subprocess
import sys

NEW_LINE = os.linesep
SHOW_COMMAND = False
RETAIN_TEMPORARY_FILES = False

# object to hold choice information for specific option
class Choice:
  label = None
  description = None
  is_default = False

  def __init__(self, label, description, is_default):
    self.label = label
    self.description = description
    self.is_default = is_default


# object to hold option/parameter/property information
class Option:
  label = None
  description = None
  choice_list = []
  accept_multiple = False
  sample_value_list = []
  is_required = False

  def __init__(self, label, description, choice_list, accept_multiple, sample_value_list, is_required):
    self.label = label
    self.description = description
    self.choice_list = choice_list
    self.accept_multiple = accept_multiple
    self.sample_value_list = sample_value_list
    self.is_required = is_required


# object to hold request information
class Request:
  common_name = ''
  output_directory = ''
  output_type_list = []
  key_algorithm = ''
  digest_algorithm = ''
  validity_days = ''
  country_code = ''
  state_name = ''
  locality_name = ''
  organization_name = ''
  organization_unit_name = ''
  email_address = ''
  alternative_dns_list = []
  alternative_ip_list = []
  keystore_alias = ''
  keystore_password = ''
  has_keystore_type = False

  # constructor
  def __init__(self):
    self.common_name = ''
    self.output_directory = ''
    self.output_type_list = []
    self.key_algorithm = ''
    self.digest_algorithm = ''
    self.validity_days = ''
    self.country_code = ''
    self.state_name = ''
    self.locality_name = ''
    self.organization_name = ''
    self.organization_unit_name = ''
    self.email_address = ''
    self.alternative_dns_list = []
    self.alternative_ip_list = []
    self.keystore_alias = ''
    self.keystore_password = ''
    self.has_keystore_type = False

  # to string
  def __str__(self):
    return 'Request {' + NEW_LINE + \
      '  common_name: ' + self.common_name + NEW_LINE + \
      '  output_directory: ' + self.output_directory + NEW_LINE + \
      '  output_type_list: ' + str(self.output_type_list) + NEW_LINE + \
      '  key_algorithm: ' + self.key_algorithm + NEW_LINE + \
      '  digest_algorithm: ' + self.digest_algorithm + NEW_LINE + \
      '  validity_days: ' + str(self.validity_days) + NEW_LINE + \
      '  country_code: ' + self.country_code + NEW_LINE + \
      '  state_name: ' + self.state_name + NEW_LINE + \
      '  locality_name: ' + self.locality_name + NEW_LINE + \
      '  organization_name: ' + self.organization_name + NEW_LINE + \
      '  organization_unit_name: ' + self.organization_unit_name + NEW_LINE + \
      '  email_address: ' + self.email_address + NEW_LINE + \
      '  alternative_dns_list: ' + str(self.alternative_dns_list) + NEW_LINE + \
      '  alternative_ip_list: ' + str(self.alternative_ip_list) + NEW_LINE + \
      '  keystore_alias: ' + self.keystore_alias + NEW_LINE + \
      '  keystore_password: ' + self.keystore_password + NEW_LINE + \
      '  has_keystore_type: ' + str(self.has_keystore_type) + NEW_LINE + \
      '}'


# constants
APP_NAME = 'python3 ' + sys.argv[0]
SAMPLE_FILE_NAME = 'sample.txt'

VALIDITY_DEFAULT = 365
VALIDITY_MINIMUM = 1

PASSWORD_DEFAULT = '123456'
PASSWORD_MINIMUM_LENGTH = 6;

CONFIG_DN_PROPERTY_WIDTH = 22
CONFIG_SAN_PROPERTY_WIDTH = 6

OUTPUT_TYPE_KEYSTORE_LIST = [ 'pkcs12', 'jceks', 'jks', 'all' ]

# info log message templates
INFO_VERIFY = 'Verifying %s %s'
INFO_SET_DEFAULT_VALUE = '%s is not specified. Use default value "%s"'

# error log message templates
ERROR_HELP = 'Get more help by running ' + APP_NAME + ' -h';
ERROR_INVALID_OPTION = 'Invalid option "%s"'
ERROR_INVALID_VALUE = 'Invalid value "%s"'
ERROR_INVALID_VALUE_OPTION = 'Invalid value "%s" for option "%s"'
ERROR_OPTION_NO_VALUE = 'Missing value for option "%s"'
ERROR_COMMAND = 'Error running command "%s"';

# regex patterns
COUNTRY_CODE_PATTERN = re.compile(r'^[A-Za-z]{2}$')
EMAIL_PATERN = re.compile(r'^[a-zA-Z0-9_.+-]+@([a-zA-Z0-9]+[a-zA-Z0-9-.])*[a-zA-Z0-9]+$')
DNS_PATTERN = re.compile(r'^([a-zA-Z0-9]+[a-zA-Z0-9-.])*[a-zA-Z0-9]+$')
IP_PATTERN = re.compile(r'^((25[0-5]|2[0-4][0-9]|[1][0-9][0-9]|[1-9][0-9]|[0-9])\.){3}(25[0-4]|2[0-4][0-9]|[1][0-9][0-9]|[1-9][0-9]|[0-9])$')
KEYSTORE_PATTERN = re.compile(r'^pkcs12|jceks|jks$')

# choices of output type
OUTPUT_TYPE_CHOICE_LIST = [
  Choice('cert', 'Self-signed certficate (.crt) with private key (.key).', True),
  Choice('pkcs12', 'Self-signed certficate in PKCS#12 format (.p12) with private key included.', False),
  Choice('jceks', 'Self-signed certficate in JCEKS format (.jceks) with private key included.', False),
  Choice('jks', 'Self-signed certficate in JKS format (.jks) with private key included.', False),
  Choice('csr', 'Certificate Signing Request only (.csr) only.', False),
  Choice('all', 'Generate all options above.', False)
]

# choices of key algorithm
ALGORITHM_KEY_CHOICE_LIST = [
  Choice('rsa2048', 'RSA 2048 bits.', True),
  Choice('rsa4096', 'RSA 4096 bits.', False),
  Choice('ec256', 'ECDSA with P-256 (secp256r1) curve.', False),
  Choice('ec384', 'ECDSA with P-384 (secp384r1) curve.', False),
  Choice('ec521', 'ECDSA with P-521 (secp521r1) curve.', False),
  Choice('x25519', 'X25519.', False)
]

# choices of digest algorithm
ALGORITHM_DIGEST_CHOICE_LIST = [
  Choice('sha256', 'SHA-256.', True),
  Choice('sha384', 'SHA-384.', False),
  Choice('sha512', 'SHA-512.', False)
]
# basic options
BASIC_OPTION_LIST = [
  Option('cn', 'Common name or server name or domain name for the certificate subject info.', None, False, [ 'local.host' ], True),
  Option('od', 'Output directory used to place the output files. Default is same as common name.', None, False, [ '/home/user/certs' ], False),
  Option('ot', 'Output types. Available choices:', OUTPUT_TYPE_CHOICE_LIST, True, [ 'pkcs12', 'jks' ], False),
  Option('ak', 'Asymmetric key algorithm to generate_and_verify the keys. Available choices:', ALGORITHM_KEY_CHOICE_LIST, False, [ 'ec384' ], False),
  Option('ad', 'Digest algorithm for certificate signature digest. Available choices:', ALGORITHM_DIGEST_CHOICE_LIST, False, [ 'sha384' ], False),
  Option('vd', 'Certificate validity period in days. Default is %d. Minimum is %d.' % (VALIDITY_DEFAULT, VALIDITY_MINIMUM), None, False, [ '30' ], False)
]

# additiona options on certificate info, will be prompt to set or skip
INFO_OPTION_LIST = [
  Option('cc', 'Country code for certificate. Only two characters allowed.', None, False, [ 'MY' ], False),
  Option('st', 'State name for the certificate subject info.', None, False, [ 'Selangor' ], False),
  Option('lo', 'Locality or city name for the certificate subject info.', None, False, [ 'Cyberjaya' ], False),
  Option('or', 'Organization name for the certificate subject info.', None, False, [ 'Company A' ], False),
  Option('ou', 'Organization unit name for the certificate subject info.', None, False, [ 'Department B' ], False),
  Option('em', 'e-mail address for the certificate subject info.', None, False, [ 'security@company.com' ], False)
]

# additiona options on SAN, will be prompt to set or skip
ALTERNATIVE_OPTION_LIST = [
  Option('dn', 'DNS name for certificate Subject Alternative Name. Common name will be automatically added.', None, True, [ 'localhost', 'host.local' ], False),
  Option('ip', 'IP address for certifcate Subject Alternative Name.', None, True, [ '127.0.0.1', '127.0.1.1' ], False)
]

# additiona options on keystore information, will be prompt to set or skip
KEYSTORE_OPTION_LIST = [
  Option('ka', 'Keystore alias. Required for output type pkcs12, jceks and/or jks. Default is same as common name.', None, False, [ 'local' ], False),
  Option('kp', 'Keystore password. Required for output type pkcs12, jceks and/or jks. At least %d characters. Default is %s' % (PASSWORD_MINIMUM_LENGTH, PASSWORD_DEFAULT), None, False, [ 'password' ], False)
]

error_count = 0
exit_on_error = False
keytool_ok = False


def log(message):
  timestamp = datetime.now().astimezone()
  print(timestamp.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + timestamp.strftime(' %z') + message + ' ...')


# log info message
def log_info(message):
  log(' [INFO ] ' + message)


# log warning message
def log_warn(message):
  log(' [WARN ] ' + message)


# log error message then exit
def log_err(message):
  global error_count
  log(' [ERROR ] ' + message)
  log('See help (-h) for further information')
  sys.exit(1)


# run command as a sub process
def run_command(command, show_output = False, return_output = False, stderr_to_stdout = False, stdin = None):
  # show command if SHOW_COMMAND set to True
  if SHOW_COMMAND:
    command_str = ''
    for param in command:
      if ' ' in param:
        command_str += "'" + param + "' "
      else:
        command_str += param + ' '

    log_info('Command to be executed: ' + command_str[:-1])

  # default no to show or return any output or error
  command_output = subprocess.DEVNULL
  command_error = subprocess.DEVNULL

  # show output and error to stdout and stderr
  if show_output:
    command_output = sys.stdout
    command_error = sys.stderr

  # return output and error to process object
  elif return_output:
    command_output = subprocess.PIPE
    command_error = subprocess.PIPE

  # redirect error stream to output stream
  if stderr_to_stdout:
    command_error = command_output

  process = subprocess.run(command, stdout = command_output, stderr = command_error, input = stdin)

  # exit if error
  if process.returncode != 0:
    log_err(ERROR_COMMAND % ' '.join(command))

  # return exit code and output
  elif return_output:
    return process.returncode, process.stdout;

  # return exit code only
  return process.returncode, None


# geenrate choice list for help message, sample and prompt
def generate_choice_list(choice_list, prefix, numbering):
  choice_list_str = ''
  for i in range(len(choice_list)):
    choice = choice_list[i]
    choice_str = prefix
    if numbering:
      choice_str += (str(i + 1) + '.').ljust(6)

    choice_str += choice.label.ljust(10) + choice.description
    if choice.is_default:
      choice_str += ' [Default]'

    choice_list_str += choice_str + NEW_LINE

  return choice_list_str


# show help message
def show_help():
  option_list_str = ''
  for option in (BASIC_OPTION_LIST + INFO_OPTION_LIST + ALTERNATIVE_OPTION_LIST + KEYSTORE_OPTION_LIST):
    option_str = '  -' + option.label.ljust(5) + option.description + NEW_LINE
    if option.choice_list:
      option_str += generate_choice_list(option.choice_list, '          ', False)

    option_list_str += option_str

  print('''Utility to create Self Sign Certificate. Available in 3 modes, interactive (default), non-interactive and file.
* OpenSSL is required to generate_and_verify keys, CSR and certificate in PEM and PKCS#12 formats.
* In addition to JCEKS and JKS format, Java KeyTool also required.

Usage:
  Prompt mode     : ${SCRIPT_NAME} -p
  File mode       : ${SCRIPT_NAME} <-f file>...
  Parameter mode  : ${SCRIPT_NAME} -cn string [-od directory] [-ot string]... [-ak string] [-ad string] [-vd number] [-cc string] [-st string] [-lo string] [-or string] [-ou string] [-em string] [-dn string]... [-ip string]... [-ka string] [-kp string]
  Generate sample : ${SCRIPT_NAME} -s
  Help            : ${SCRIPT_NAME} -h

  -h    Show this help message.
  -s    Generate sample input file, ${SAMPLE_FILE_NAME}.
  -f    Generate output based on parameters in a file.
  -p    Generate output based on prompt response.
${OPTIONS}'''.replace('${SAMPLE_FILE_NAME}', SAMPLE_FILE_NAME).replace('${SCRIPT_NAME}', APP_NAME).replace('${OPTIONS}', option_list_str))


# generate sample input file for option -f
def generate_sample():
  sample_content = '# To use this file, run "' + APP_NAME + ' -f ' + SAMPLE_FILE_NAME + '"' + NEW_LINE + NEW_LINE
  for option in (BASIC_OPTION_LIST + INFO_OPTION_LIST + ALTERNATIVE_OPTION_LIST + KEYSTORE_OPTION_LIST):
    option_str = '# ' + option.description + NEW_LINE
    if option.choice_list:
      option_str += generate_choice_list(option.choice_list, '#   ', False)

    for sample_value in option.sample_value_list:
      sample_value_str = option.label + ' = ' + sample_value
      if not option.is_required:
        sample_value_str = '#' + sample_value_str
      option_str += sample_value_str + NEW_LINE

    sample_content += option_str + NEW_LINE

  if os.path.exists(SAMPLE_FILE_NAME):
    os.remove(SAMPLE_FILE_NAME)

  with open(SAMPLE_FILE_NAME, 'w') as sample_file_writer:
    sample_file_writer.write(sample_content)

  print('Sample input file generated. ' + SAMPLE_FILE_NAME)


# check if input matches with any choice label
def is_valid_choice(value, choice_list):
  for choice in choice_list:
    if choice.label == value:
      return True

  return False


# get all choices label if input is 'all'
def get_all_choices(choice_list):
  all_values = []
  for choice in choice_list:
    if 'all' != choice.label:
      all_values.append(choice.label)
  return all_values


# validate new input
# exit if fail validation
def validate_option(request, option, value):
  # reject if no value or blank
  if not value:
    log_err(ERROR_OPTION_NO_VALUE % option)

  # accept if value not start with hyphen
  elif value.startswith('-'):
    log_err(ERROR_INVALID_VALUE % value)

  # add common name to DNS SAN list
  if '-cn' == option:
    request.common_name = value
    request.alternative_dns_list.append(value)

  elif '-od' == option:
    request.output_directory = value

  elif '-ot' == option:
    if value in OUTPUT_TYPE_KEYSTORE_LIST:
      request.has_keystore_type = True

    if not is_valid_choice(value, OUTPUT_TYPE_CHOICE_LIST):
      log_err(ERROR_INVALID_VALUE_OPTION % (value, 'Output type'))
    elif 'all' == value:
      request.output_type_list += get_all_choices(OUTPUT_TYPE_CHOICE_LIST)
    else:
      request.output_type_list.append(value)

  # accept single valid choice
  elif '-ak' == option:
    if not is_valid_choice(value, ALGORITHM_KEY_CHOICE_LIST):
      log_err(ERROR_INVALID_VALUE_OPTION % (value, 'Key algorithm'))
    else:
      request.key_algorithm = value

  # accept single valid choice
  elif '-ad' == option:
    if not is_valid_choice(value, ALGORITHM_DIGEST_CHOICE_LIST):
      log_err(ERROR_INVALID_VALUE_OPTION % (value, 'Digest algorithm'))
    else:
      request.digest_algorithm = value

  # accept if equals or more than minimum
  elif '-vd' == option:
    try:
      number_value = int(value)
      if number_value < VALIDITY_MINIMUM:
        raise
      else:
        request.validity_days = value
    except:
      log_err(ERROR_INVALID_VALUE_OPTION % (value, 'Validity period'))

  # accept if 2 letters
  elif '-cc' == option:
    if not COUNTRY_CODE_PATTERN.search(value):
      log_err(ERROR_INVALID_VALUE_OPTION % (value, 'Country code'))
    else:
      request.country_code = value

  elif '-st' == option:
    request.state_name = value

  elif '-lo' == option:
    request.locality_name = value

  elif '-or' == option:
    request.organization_name = value

  elif '-ou' == option:
    request.organization_unit_name = value

  # accept if value satisfy email address regex
  elif '-em' == option:
    if not EMAIL_PATERN.search(value):
      log_err(ERROR_INVALID_VALUE_OPTION % (value, 'e-mail address'))
    else:
      request.email_address = value

  # accept if value satisfy dns name regex
  elif '-dn' == option:
    if not DNS_PATTERN.search(value):
      log_err(ERROR_INVALID_VALUE_OPTION % (value, 'DNS name'))
    else:
      request.alternative_dns_list.append(value)

  # accept if value satisfy ip address regex
  elif '-ip' == option:
    if not IP_PATTERN.search(value):
      log_err(ERROR_INVALID_VALUE_OPTION % (value, 'IP address'))
    else:
      request.alternative_ip_list.append(value)

  elif '-ka' == option:
    request.keystore_alias = value

  # accept if more than 6 characters
  elif '-kp' == option:
    if len(value) < PASSWORD_MINIMUM_LENGTH:
      log_err('Password too short. Minimum %d characters' % PASSWORD_MINIMUM_LENGTH)
    else:
      request.keystore_password = value

  # exit if invalid
  else:
    log_err(ERROR_INVALID_OPTION % option)


# get defalult choice
def get_default_choice(choices):
  for choice in choices:
    if choice.is_default:
      return choice.label

  return None


# set default values if not specified. available in help message, sample input file and prompt
def set_default_values(request):
  # set output directory same as common name
  if not request.output_directory:
    common_name = request.common_name
    log_info(INFO_SET_DEFAULT_VALUE % ('Output directory', common_name))
    request.output_directory = common_name

  # set default output type based on choices information
  if len(request.output_type_list) == 0:
    default_output_type = get_default_choice(OUTPUT_TYPE_CHOICE_LIST)
    log_info(INFO_SET_DEFAULT_VALUE % ('Output type', default_output_type))
    request.output_type_list.append(default_output_type)

  # set default key algorithm based on choices information
  if not request.key_algorithm:
    default_key_algorithm = get_default_choice(ALGORITHM_KEY_CHOICE_LIST)
    log_info(INFO_SET_DEFAULT_VALUE % ('Key algorithm', default_key_algorithm))
    request.key_algorithm = default_key_algorithm

  # set default digest algorithm based on choices information
  if not request.digest_algorithm:
    default_digest_algorithm = get_default_choice(ALGORITHM_DIGEST_CHOICE_LIST)
    log_info(INFO_SET_DEFAULT_VALUE % ('Digest algorithm', default_digest_algorithm))
    request.digest_algorithm = default_digest_algorithm

  # set default validity period
  if not request.validity_days:
    default_validity_days = VALIDITY_DEFAULT
    log_info(INFO_SET_DEFAULT_VALUE % ('Validity period', default_validity_days))
    request.validity_days = default_validity_days

  if request.has_keystore_type:
    # set default keystore alias
    if not request.keystore_alias:
      default_keystore_alias = request.common_name
      log_info(INFO_SET_DEFAULT_VALUE % ('Keystore alias', default_keystore_alias))
      request.keystore_alias = default_keystore_alias

    # set default keystore password
    if not request.keystore_password:
      default_keystore_password = PASSWORD_DEFAULT
      log_info(INFO_SET_DEFAULT_VALUE % ('Keystore password', default_keystore_password))
      request.keystore_password = default_keystore_password


# prompt basic and additional options
def prompt_options(request, option_list):
  for option in option_list:
    # show description
    print(option.description)

    # multi value prompt message
    if option.accept_multiple:
      if option.choice_list:
        prompt_message = 'Choose at least one, separate with comma'
      else:
        prompt_message = 'Enter one or more value, separate with comma'

    # single value prompt message
    else:
      if option.choice_list:
        prompt_message = 'Choose one'
      else:
        prompt_message = 'Enter a value'

    # optional prompt message
    if not option.is_required:
      prompt_message += ', or skip'

    # show choices
    if option.choice_list:
      print(generate_choice_list(option.choice_list, '  ', True))

    # get value
    value = input(prompt_message + ': ').strip()
    if not value and option.is_required:
      log_err('Please enter a value')

    if value:
      # choice based response
      if option.choice_list:
        choices = value.split(',')
        if not option.accept_multiple and len(choices) > 1:
          log_err('Only single choice allowed')

        for choice in choices:
          try:
            option_number = int(choice.strip())
            if option_number not in range(1, len(option.choice_list) + 1):
              raise

            choice_value = option.choice_list[option_number - 1].label
            validate_option(request, '-' + option.label, choice_value)
          except:
            log_err(ERROR_INVALID_VALUE % choice)

      # non choice based response
      else:
        if not option.accept_multiple:
          validate_option(request, '-' + option.label, value)
        else:
          values = value.split(',')
          for val in values:
            validate_option(request, '-' + option.label, val.strip())


# prompt for request information
def prompt():
  request = Request()

  # prompt basic options
  prompt_options(request, BASIC_OPTION_LIST)

  # ask before prompt certification info options
  update_info = input('Add certificate info? (Y/N): ').strip()
  if update_info and 'Y' == update_info[0].upper():
    prompt_options(request, INFO_OPTION_LIST)

  # ask before prompt SAN options
  update_alternative = input('Add alternative DNS and/or IP? (Y/N): ').strip()
  if update_alternative and 'Y' == update_alternative[0].upper():
    prompt_options(request, ALTERNATIVE_OPTION_LIST)

  # ask before prompt keystore info options
  if request.has_keystore_type:
    update_keystore = input('Add keystore info? (Y/N): ').strip()
    if update_keystore and 'Y' == update_keystore[0].upper():
      prompt_options(request, KEYSTORE_OPTION_LIST)

  # set default values for options without response
  set_default_values(request)
  return request


# read input file
def read_input_file(input_file):
  log_info('Reading input file ' + input_file)
  request = Request()
  with open(input_file, 'r') as input_file_reader:
    for line in input_file_reader:
      line = line.strip()
      if not line or line.startswith('#'):
        continue

      # skip invalid line
      prop = line.split('=', 1)
      if len(prop) != 2:
        log_warn('Invalid line. ' + line)
        continue

      option = prop[0].strip()
      value = prop[1].strip()
      validate_option(request, '-' + option, value)

  # exit if no common name
  if not request.common_name:
    log_err('Missing common name.')
  else:
    set_default_values(request)
    return request

  return None


# generate OpenSSL config file for CSR and certificate
def generate_conf(request, config_file):
  log_info('Generating OpenSSL config file')

  conf_content = '''[ req ]
prompt             = no
req_extensions     = ext
distinguished_name = dn

[ ext ]
keyUsage         = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName   = @san

[ dn ]
'''

  conf_content += 'commonName'.ljust(CONFIG_DN_PROPERTY_WIDTH) + ' = ' + request.common_name + NEW_LINE

  if request.country_code:
    conf_content += 'countryName'.ljust(CONFIG_DN_PROPERTY_WIDTH) + ' = ' + request.country_code + NEW_LINE

  elif request.state_name:
    conf_content += 'stateOrProvinceName'.ljust(CONFIG_DN_PROPERTY_WIDTH) + ' = ' + request.state_name + NEW_LINE

  elif request.locality_name:
    conf_content += 'localityName'.ljust(CONFIG_DN_PROPERTY_WIDTH) + ' = ' + request.locality_name + NEW_LINE

  elif request.organization_name:
    conf_content += 'organizationName'.ljust(CONFIG_DN_PROPERTY_WIDTH) + ' = ' + request.organization_name + NEW_LINE

  elif request.organization_unit_name:
    conf_content += 'organizationalUnitName'.ljust(CONFIG_DN_PROPERTY_WIDTH) + ' = ' + request.organization_unit_name + NEW_LINE

  elif request.email_address:
    conf_content += 'emailAddress'.ljust(CONFIG_DN_PROPERTY_WIDTH) + ' = ' + request.email_address + NEW_LINE

  conf_content += NEW_LINE + '[ san ]' + NEW_LINE

  # remove duplicate and write with number from 0
  dns_list = list(dict.fromkeys(request.alternative_dns_list))
  for i in range(len(dns_list)):
    conf_content += ('DNS.' + str(i)).ljust(CONFIG_SAN_PROPERTY_WIDTH) + ' = ' + dns_list[i] + NEW_LINE

  ip_list = list(dict.fromkeys(request.alternative_ip_list))
  for i in range(len(request.alternative_ip_list)):
    conf_content += ('IP.' + str(i)).ljust(CONFIG_SAN_PROPERTY_WIDTH) + ' = ' + ip_list[i] + NEW_LINE

  if os.path.exists(config_file):
    os.remove(config_file)

  with open(config_file, 'w') as conf_writer:
    conf_writer.write(conf_content)


# generate key using OpenSSL
def generate_key(request, param_file, key_file):
  log_info('Generating key')

  if os.path.exists(param_file):
    os.remove(param_file)

  if os.path.exists(key_file):
    os.remove(key_file)

  if 'rsa2048' == request.key_algorithm:
    command_list = [
      [ 'openssl', 'genrsa', '-out', key_file, '2048' ]
    ]

  elif 'rsa4096' == request.key_algorithm:
    command_list = [
      [ 'openssl', 'genrsa', '-out', key_file, '4096' ]
    ]

  # for ECDSA, parameter file is required
  elif request.key_algorithm.startswith('ec'):
    ecdsa_curve = request.key_algorithm.replace('ec', 'secp') + 'r1'
    command_list = [
      [ 'openssl', 'ecparam', '-name', ecdsa_curve, '-out', param_file, '-outform', 'pem' ],
      [ 'openssl', 'ecparam', '-genkey', '-noout', '-in', param_file, '-out', key_file, '-outform', 'pem' ]
    ]

  elif 'ec384' == request.key_algorithm:
    command_list = [
      [ 'openssl', 'ecparam', '-name', 'secp384r1', '-out', param_file, '-outform', 'pem' ],
    ]

  elif 'ec521' == request.key_algorithm:
    command_list = [
      [ 'openssl', 'ecparam', '-name', 'secp521r1', '-out', param_file, '-outform', 'pem' ],
      [ 'openssl', 'ecparam', '-genkey', '-noout', '-in', param_file, '-out', key_file, '-outform', 'pem' ]
    ]

  elif 'x25519' == request.key_algorithm:
    command_list = [
      [ 'openssl', 'genpkey', '-algorithm', 'x25519', '-out', key_file ]
    ]

  for command in command_list:
    run_command(command)


# generate CSR using OpenSSL, using config and key files
def generate_csr(key_algorithm, digest_algorithm, config_file, key_file, csr_file):
  log_info('Generating request')

  if os.path.exists(csr_file):
    os.remove(csr_file)

  command = [ 'openssl', 'req', '-new', '-config', config_file, '-key', key_file, '-out', csr_file ]

  command.append('-' + digest_algorithm)

  run_command(command)


# verify CSR using OpenSSL
def verify_csr(csr_file):
  log_info(INFO_VERIFY % ('request', csr_file))
  command = [ 'openssl', 'req', '-text', '-noout', '-in', csr_file ]
  run_command(command, show_output = True)


# generate certificate using OpenSSL, using config, key and csr files
def generate_cert(key_algorithm, digest_algorithm, config_file, key_file, csr_file, crt_file, validity_days):
  log_info('Generating certificate')

  if os.path.exists(crt_file):
    os.remove(crt_file)

  command = [ 'openssl', 'x509', '-req', '-extfile', config_file, '-extensions', 'ext', '-in', csr_file, '-signkey', key_file, '-out', crt_file, '-outform', 'pem', '-days', str(validity_days) ]

  command.append('-' + digest_algorithm)

  run_command(command)


# verify certificate using OpenSSL
def verify_cert(cert_file):
  log_info(INFO_VERIFY % ('certificate', cert_file))
  command = [ 'openssl', 'x509', '-text', '-noout', '-in', cert_file ]
  run_command(command, show_output = True)


# generate PKCS#12 format using OpenSSL, using key and certificate files
def generate_pkcs12(key_file, cert_file, pkcs12_file, keystore_password, keystore_alias):
  log_info('Generating PKCS#12 format')

  if os.path.exists(pkcs12_file):
    os.remove(pkcs12_file)

  command = [ 'openssl', 'pkcs12', '-export', '-in', cert_file, '-inkey', key_file, '-out', pkcs12_file, '-passout', 'pass:' + keystore_password, '-name', keystore_alias ]
  run_command(command, show_output = True)


# verify PKCS#12 using OpenSSL
def verify_pkcs12(pkcs12_file, keystore_password):
  log_info(INFO_VERIFY % ('PKCS#12', pkcs12_file))

  command = [ 'openssl', 'pkcs12', '-in', pkcs12_file, '-passin', 'pass:' + keystore_password, '-nodes', '-nokeys' ]
  _, output = run_command(command, return_output = True)

  command = [ 'openssl', 'x509', '-text', '-noout' ]
  run_command(command, show_output = True, stdin = output)


# convert PKCS#12 file to JCEKS or JKS using Java KeyTool
def generate_keystore(pkcs12_file, keystore_file, keystore_type, keystore_password, keystore_alias):
  log_info('Converting PKCS#12 format to %s format' % keystore_type.upper())

  if os.path.exists(keystore_file):
    os.remove(keystore_file)

  command = [ 'keytool', '-importkeystore', '-srckeystore', pkcs12_file, '-srcstorepass', keystore_password, '-srcalias', keystore_alias,
             '-destkeystore', keystore_file, '-deststoretype', keystore_type, '-deststorepass', keystore_password, '-destalias', keystore_alias ]
  run_command(command)


# verify JCEKS or JKS using Java KeyTool
def verify_keystore(keystore_file, keystore_password):
  log_info(INFO_VERIFY % ('keystore', keystore_file))
  command = [ 'keytool', '-list', '-v', '-keystore', keystore_file, '-storepass', keystore_password ]
  run_command(command, show_output = True)


def generate_and_verify(request):
  temp_file_list = []

  # create output directory if not exists
  if not os.path.exists(request.output_directory):
    os.makedirs(request.output_directory)

  base_file = request.output_directory + '/' + request.common_name

  # generate OpenSSL config file
  config_file = base_file + '.conf'
  temp_file_list.append(config_file)
  generate_conf(request, config_file)

  # generate key file
  param_file = base_file + '.param'
  temp_file_list.append(param_file)

  key_file = base_file + '.key'
  cert_file = base_file + '.crt'
  if not 'cert' in request.output_type_list:
    cert_file += '.tmp'
    key_file += '.tmp'
    temp_file_list.append(cert_file)
    temp_file_list.append(key_file)

  generate_key(request, param_file, key_file)

  csr_file = base_file + '.csr'
  if not 'csr' in request.output_type_list:
    temp_file_list.append(csr_file)

  # generate and verify CSR
  generate_csr(request.key_algorithm, request.digest_algorithm, config_file, key_file, csr_file)
  if 'csr' in request.output_type_list:
    verify_csr(csr_file)

  # generate and verify certificate
  if not (len(request.output_type_list) == 1 and 'csr' in request.output_type_list):
    generate_cert(request.key_algorithm, request.digest_algorithm, config_file, key_file, csr_file, cert_file, request.validity_days)
    if 'cert' in request.output_type_list:
      verify_cert(cert_file)

  # generate and verify PKCS#12
  if 'pkcs12' in request.output_type_list or 'jceks' in request.output_type_list or 'jks' in request.output_type_list:
    pkcs12_file = base_file + '.p12'
    generate_pkcs12(key_file, cert_file, pkcs12_file, request.keystore_password, request.keystore_alias)
    if 'pkcs12' in request.output_type_list:
      verify_pkcs12(pkcs12_file, request.keystore_password)

  # generate and verify JCEKS
  if keytool_ok and 'jceks' in request.output_type_list:
    jceks_file = base_file + '.jceks'
    generate_keystore(pkcs12_file, jceks_file, 'jceks', request.keystore_password, request.keystore_alias)
    verify_keystore(jceks_file, request.keystore_password)

  # generate and verify JKS
  if keytool_ok and 'jks' in request.output_type_list:
    jks_file = base_file + '.jks'
    generate_keystore(pkcs12_file, jks_file, 'jks', request.keystore_password, request.keystore_alias)
    verify_keystore(jks_file, request.keystore_password)

  # remove temporary files
  if not RETAIN_TEMPORARY_FILES:
    log_info('Cleaning up temporary files')
    for temp_file in temp_file_list:
      if os.path.exists(temp_file):
        os.remove(temp_file)


# check OpenSSL command, exit if error
def check_openssl():
  log_info('Checking OpenSSL')
  run_command([ 'openssl', 'version' ], show_output = True)

  log_info('OpenSSL OK')


# check Java and KeyTool, warn if error and skip generating JCEKS and JKS
def check_keytool():
  global keytool_ok
  log_info('Checking Java and KeyTool')
  exit_code, _ = run_command([ 'java', '-version' ], show_output = True)
  if exit_code == 0:
    exit_code, _ = run_command([ 'keytool', '-h' ])
    if exit_code == 0:
      keytool_ok = True
      log_info('Java KeyTool OK')
      return

  log_warn('Something wrong Java KeyTool, JCEKS and/or JKS will not be generated')


if __name__ == '__main__':
  args = sys.argv
  # args += [ '-f', 'sample.txt' ]
  # args += [ '-p' ]
  # args += [ '-s' ]
  # args += [ '-h' ]
  # args += [ '-cn', 'abc.com' ]

  # show help message then exit
  if '-h' in args:
    show_help()
    sys.exit(0)

  # generate sample input file then exit
  if '-s' in args:
    generate_sample()
    sys.exit(0)

  log_info('Started')
  request_list = {}

  # prompt for input to generate
  if '-p' in args:
    exit_on_error = True
    request = prompt()
    request_list[request.common_name] = request

  # generate based on input file(s)
  # skip if file not found
  # skip if file contains similar common name
  elif '-f' in args:
    input_file_list = []
    for i in range(1, len(args) - 1):
      if '-f' == args[i]:
        prop_file = args[i + 1]
        if os.path.exists(prop_file):
          input_file_list.append(args[i + 1])
        else:
          log_err('File not found. ' + prop_file)

    if len(input_file_list) > 0:
      log_info('Total %d input files' % len(input_file_list))

      for input_file in input_file_list:
        request = read_input_file(input_file)
        if request:
          if request.common_name in request_list:
            log_warn('Duplicate common name. Skip')
          else:
            request_list[request.common_name] = request
    else:
      log_warn('No files to be processed')

  # generate based on cli parameters given
  # exit if missing common name
  elif '-cn' in args:
    request = Request()
    for i in range(1, len(args) - 1, 2):
      validate_option(request, args[i].strip(), args[i + 1].strip())

    if not request.common_name:
      log_err('Missing common name')
    else:
      set_default_values(request)
      request_list[request.common_name] = request

  # show help message then exit
  else:
    show_help()
    sys.exit(0)

  if request_list:
    # check OpenSSL, exit if error
    exit_on_error = True
    check_openssl()

    # check Java and KeyTool
    exit_on_error = False
    check_keytool()

    # process request(s)
    exit_on_error = True
    for request in request_list.values():
      generate_and_verify(request)

  log_info('Finished')
