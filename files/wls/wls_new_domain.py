# By Azman Kudus
# Last update on 17 Jan 2022

from datetime import datetime
import sys
import traceback
from java.io import FileInputStream
from java.util import Properties
from wlstModule import *

ENABLE_TRACEBACK = False

def log(level, message):
  print '%s [%s] %s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S %z'), level, message)


def log_info(message):
  log('INFO ', message)


def log_warn(message):
  log('WARN ', message)


def log_error(message):
  log('ERROR', message)
  traceback.print_exc()



log_info('Started')

TEMPLATE_NAME = 'Basic WebLogic Server Domain'
NODEMANAGER_USERNAME = 'nodemanager'
WEBLOGIC_USERNAME = 'weblogic'

log_info('Reading properties file ' + PROPERTIES_FILE)
PROPERTIES_FILE = sys.argv[1]
fis = FileInputStream(PROPERTIES_FILE)
props = Properties()
props.load(fis)

java_home = props.get('java.home')
domain_base = props.get('domain.base')

machine_name = props.get('machine.name')
domain_name = props.get('domain.name')
production_mode = props.get('production.mode')

nodemanager_host = props.get('nodemanager.host')
nodemanager_port = props.get('nodemanager.port')
nodemanager_password = props.get('nodemanager.password')

adminserver_host = props.get('adminserver.host')
adminserver_port = props.get('adminserver.port')
adminserver_password = props.get('adminserver.password')

log_info('Loading template "' + TEMPLATE_NAME + '"')
selectTemplate(TEMPLATE_NAME)
loadTemplates()

log_info('Setting domain and JDK info')
setOption('DomainName', domain_name)
setOption('JavaHome', java_home)
setOption('NodeManagerType', 'PerDomainNodeManager')

if production_mode and 'y' == production_mode.lower():
  setOption('ServerStartMode','prod')

log_info('Setting node manager host, port and password')
cd('/')
create(domain_name, 'SecurityConfiguration')
cd('/SecurityConfiguration/' + domain_name)
set('NodeManagerUsername', 'nodemanager')
set('NodeManagerPasswordEncrypted', nodemanager_password)
cd('/')
create(machine_name, 'Machine')
cd('/Machine/' + machine_name)
create(machine_name, 'NodeManager')
cd('NodeManager/' + machine_name)
set('ListenAddress', nodemanager_host)
set('ListenPort', int(nodemanager_port))
set('NMType', 'Plain')
set('UserName', NODEMANAGER_USERNAME)
set('PasswordEncrypted', nodemanager_password)

log_info('Setting admin server host, port and password')
cd('/Servers/AdminServer')
set('ListenAddress', adminserver_host)
set('ListenPort', int(adminserver_port))  
cd('/Security/base_domain/User/' + WEBLOGIC_USERNAME)
set('Password', adminserver_password)

log_info('Creating domain "%s" in "%s"' % (domain_name, domain_base))
writeDomain(domain_base + '/' + domain_name)
closeTemplate()

log_info('Finished')
exit()
