#!/bin/bash

# By Azman Kudus
# Last update on 17 Jan 2022

shopt -s expand_aliases
alias awk=gawk
alias find=gfind
alias grep=ggrep
alias sed=gsed
alias tar=gtar

CURRENT_USER="$(whoami)"
SCRIPT_OWNER="$(ls -l "${0}" | cut -d' ' -f3)"
SCRIPT_DIR="$(readlink -f $(dirname "${0}"))"
SCRIPT_NAME="$(basename "${0}")"

if [ "${CURRENT_USER}" != "${SCRIPT_OWNER}" ]
then
  log_err "You are not script owner. Login as \"${SCRIPT_OWNER}\" and try again."
  exit 1
fi

umask 027

log() {
  echo "$(date +'%Y-%m-%d %H:%M:%S.%3N %z') $*"
}

log_info() {
  log "[INFO ] ${*} ..."
}

log_warn() {
  log "[WARN ] ${*} !"
}

log_err() {
  log "[ERROR] ${*} !!!"
  log "Run \"bash ${SCRIPT_NAME} -h\" to get more help"
}

print_help() {
  cat <<EOF
1. Place JDK zip file (jdk-*-V*.zip) that contains JDK tar.gz file into "install" folder.
2. Export ORACLE_BASE to the desired path. Use same path for all scripts.
3. This script will install JDK to $ORACLE_BASE/jdk. Backup if required.
4. Run this script using Bash (bash ${SCRIPT_NAME}). Better to run with nohup.
EOF
}

if [ "$1" = "--help" ] || [ "$1" = "-h" ]
then
  print_help
  exit 0
fi

log_info "Started"

INSTALLER_DIR="${SCRIPT_DIR}/install"
INSTALLER_TMP_DIR="${INSTALLER_DIR}/tmp"

if [ -z "${ORACLE_BASE}" ]
then
  log_err "ORACLE_BASE is not set"
  exit 2
fi

mkdir -p "${ORACLE_BASE}"

log_info "Starting JDK installation"
log_info "ORACLE_BASE is set to ${ORACLE_BASE}"

INSTALL_PATH="${ORACLE_BASE}/jdk"
log_info "Target installation path is ${INSTALL_PATH}"

cd "${INSTALLER_DIR}"

JDK_ZIP="$(find "${INSTALLER_DIR}" -type f -name "jdk-*-V*.zip" | head -1)"
if [ -z "${JDK_ZIP}" ]
then
  log_err "Missing JDK zip file"
  exit 3
fi
log_info "JDK zip file found. ${JDK_ZIP}"

JDK_GZIP="$(zipinfo "${JDK_ZIP}" | grep -P 'jdk\-.*\.tar\.gz$' | awk '{print $NF}')"
if [ -z "${JDK_GZIP}" ]
then
  log_err "Missing JDK in tar.gz format within the zip file"
  exit 4
fi
log_info "JDK tar.gz found. ${JDK_GZIP}"
log_info "JDK version $(echo ${JDK_GZIP} | cut -d'-' -f2)"

log_info "Create temporary directory"
mkdir "${INSTALLER_TMP_DIR}"
cd "${INSTALLER_TMP_DIR}"

log_info "Extracting ${JDK_GZIP}  from ${JDK_ZIP}"
unzip -oqq "${JDK_ZIP}" "${JDK_GZIP}"

JDK_DIR="$(tar tzf "${JDK_GZIP}" | head -1 | sed 's/\/$//')"
log_info "Original JDK directory name is ${JDK_DIR}"

log_info "Extracting all files from ${JDK_GZIP}"
tar xzf "${JDK_GZIP}"

log_info "Deleting current JDK at ${INSTALL_PATH}"
rm -rf "${INSTALL_PATH}"

log_info "Moving from $PWD/$JDK_DIR to ${INSTALL_PATH}"
mv "${JDK_DIR}" "${INSTALL_PATH}"

log_info "Cleaning up temporary directory"
cd "${INSTALLER_DIR}"
rm -rf "${INSTALLER_TMP_DIR}"

cd "${SCRIPT_DIR}"
log_info "Finished"
