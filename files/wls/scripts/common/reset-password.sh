#!/bin/bash

USERNAME="${1}"
PASSWORD="${2}"

if [ -z "${PASSWORD}" ]
then
  read -p "Enter new password: " NEW_PASSWORD_1
  if [ -z "${NEW_PASSWORD_1}" ]
  then
    echo "ERROR!!! No password entered."
    exit 1
  fi
  
  read -p "Confirm new password: " NEW_PASSWORD_2
  if [ -z "${NEW_PASSWORD_2}" ]
  then
    echo "ERROR!!! No password entered."
    exit 2
  fi
  
  if [ "${NEW_PASSWORD_1}" != "${NEW_PASSWORD_2}" ]
  then
    echo "ERROR!!! Password not match."
    exit 3
  fi
  
  PASSWORD="${NEW_PASSWORD_2}"
fi

if [ $(echo "${PASSWORD}" | wc -m) -le 8 ]
then
  echo "ERROR!!! Password length must be at least 8 characters"
  exit 4
fi

if [ $(echo "${PASSWORD}" | grep -cP '[A-Z]') -eq 0 ] || [ $(echo "${PASSWORD}" | grep -cP '[a-z]') -eq 0 ] || [ $(echo "${PASSWORD}" | grep -cP '[0-9]') -eq 0 ] || [ $(echo "${PASSWORD}" | grep -cP "[\`~!@#$%^&*()-=_+[]\{}|;':\",./<>?]") -eq 0 ]

# source "$(dirname "${0}")/common.sh"
# if [ ${?} -ne 0 ]
# then
#   echo "ERROR!!! Problem while loading common.sh"
#   exit 1
# fi
# f_start
# 
# wlst.sh "${COMMON_DIR}/wlst_reset_password.py" "${1}"
# 
# f_finish

echo hello | grep -cP "[\`~!@#\$%^&*()-=_+[]\{}|;':\",./<>?]"

echo "\`~\!@#$%^&*()-=_+[]\{}|;':\",./<>?"
