import sys
from wlst_connect import *

OPERATION   = sys.argv[1].strip()
APP_NAME    = sys.argv[2].strip()
TARGET_NAME = sys.argv[3].strip()
APP_PATH    = sys.argv[4].strip()
PLAN_XML    = sys.argv[5].strip()

def print_app_status(entries):
  name_header = 'Application'
  target_header = 'Target'
  status_header = 'Status'

  name_max_length = len(name_header)
  target_max_length = len(target_header)
  status_max_length = len(status_header)
  
  for entry in entries:
    if len(entry[0]) > name_max_length:
      name_max_length = len(entry[0])
    
    if len(entry[1]) > target_max_length:
      target_max_length = len(entry[1])
    
    if len(entry[2]) > status_max_length:
      status_max_length = len(entry[2])
      
  print '~~~~~%s---%s---%s' % ('-' * name_max_length, '-' * target_max_length, '-' * status_max_length)
  print '~~~~~%-*s   %-*s   %-*s' % (name_max_length, name_header, target_max_length, target_header, status_max_length, status_header)
  print '~~~~~%s---%s---%s' % ('-' * name_max_length, '-' * target_max_length, '-' * status_max_length)
  for entry in entries:
    print '~~~~~%-*s   %-*s   %-*s' % (name_max_length, entry[0], target_max_length, entry[1], status_max_length, entry[2])
  print '~~~~~%s---%s---%s' % ('-' * name_max_length, '-' * target_max_length, '-' * status_max_length)

def show_app_status():
  entries = []
  app_names = APP_NAME.split(' ')
  
  as_connect()
  
  for app_name in app_names:
    serverConfig()
    targets = getMBean('/AppDeployments/'+ app_name + '/Targets').getTargets()
    
    domainRuntime()
    cd('AppRuntimeStateRuntime/AppRuntimeStateRuntime')
    cmo = cd(pwd())
    
    for target in targets:
      status = cmo.getCurrentState(app_name, target.getName())
      entries.append([app_name, target.getName(), status])
    
  as_disconnect()
  
  print_app_status(entries)
  

def start_app():
  as_connect()
  startApplication(APP_NAME)
  as_disconnect()
  

def stop_app():
  as_connect()
  stopApplication(APP_NAME)
  as_disconnect()
  

def deploy_app():
  as_connect()
  if not PLAN_XML:
    deploy(APP_NAME, targets=TARGET_NAME, path=APP_PATH, adminMode='false')
  else:
    deploy(APP_NAME, targets=TARGET_NAME, path=APP_PATH, planPath=PLAN_XML, adminMode='false')
  as_disconnect()
  

def undeploy_app():
  as_connect()
  undeploy(APP_NAME, targets=TARGET_NAME)
  as_disconnect()

  
if OPERATION == 'start':
  start_app()

elif OPERATION == 'stop':
  stop_app()

elif OPERATION == 'deploy':
  deploy_app()

elif OPERATION == 'undeploy':
  undeploy_app()

elif OPERATION == 'status':
  show_app_status()

else:
  print 'ERROR!!! Invalid operation "%s"' % OPERATION

exit()
