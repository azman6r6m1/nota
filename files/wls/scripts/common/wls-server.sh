#!/bin/bash

OPERATION="${1}"
SERVER_NAME="${2}"
SCRIPT_NAME="${SCRIPT_NAME}_${OPERATION}_${SERVER_NAME}"

source "$(dirname "${0}")/common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading common.sh"
  exit 1
fi

SERVER_DIR="${DOMAIN_HOME}/servers/${SERVER_NAME}"

case "${OPERATION}" in

  "status" )
    wlst.sh "${COMMON_DIR}/wlst_server.py" status "${SERVER_NAME}" 2>&1 | grep '^~~~~~' | sed 's/~~~~~//'
  ;;

  "start" )
    f_start
    f_log_info "Starting server ${SERVER_NAME}"
    wlst.sh "${COMMON_DIR}/wlst_server.py" start "${SERVER_NAME}"
    f_finish $?
  ;;

  "stop" )
    f_start
    f_log_info "Stopping server ${SERVER_NAME}"
    wlst.sh "${COMMON_DIR}/wlst_server.py" stop "${SERVER_NAME}"
    f_finish $?
  ;;

  "kill" )
    f_start
    f_log_info "Killing server ${SERVER_NAME}"
    wlst.sh "${COMMON_DIR}/wlst_server.py" kill "${SERVER_NAME}"
    f_finish $?
  ;;

  "cleanup" )
    f_start
    if [ ! -d "${SERVER_DIR}" ]
    then
      f_log_err "Invalid path ${SERVER_DIR}"
      exit 2
    fi

    f_log_info "Cleaning up cached files"
    rm -rf ${SERVER_DIR}/cache/*

    f_log_info "Cleaning up server staging files"
    rm -rf ${SERVER_DIR}/stage/*

    f_log_info "Cleaning up server temporary files"
    rm -rf ${SERVER_DIR}/tmp/*
    f_finish $?
  ;;

  * )
    echo "ERROR!!! Invalid operation \"${OPERATION}\""
    echo "Available operations: status, start, stop, kill, cleanup"
    exit 3
  ;;
esac
