#!/bin/bash

cat <<EOF > "start-nodemanager"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi

COMMON_ADMIN_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
export SCRIPT_NAME="\$(basename "\${0}")"

"\${COMMON_ADMIN_SCRIPT_DIR}/wls-nodemanager.sh" start
EOF
chmod u+x "start-nodemanager"

cat <<EOF > "stop-nodemanager"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi

COMMON_ADMIN_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
export SCRIPT_NAME="\$(basename "\${0}")"

"\${COMMON_ADMIN_SCRIPT_DIR}/wls-nodemanager.sh" stop
EOF
chmod u+x "stop-nodemanager"

cat <<EOF > "status-nodemanager"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi

COMMON_ADMIN_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
export SCRIPT_NAME="\$(basename "\${0}")"

"\${COMMON_ADMIN_SCRIPT_DIR}/wls-nodemanager.sh" status
EOF
chmod u+x "status-nodemanager"
