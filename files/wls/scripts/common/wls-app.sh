#!/bin/bash

OPERATION="${1}"
APP_NAME="${2}"
TARGET_NAME="${3}"
APP_PATH="${4}"
PLAN_XML="${5}"

SCRIPT_NAME="${SCRIPT_NAME}_${APP_NAME}_${TARGET_NAME}"
source "$(dirname "${0}")/common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading common.sh"
  exit 1
fi

case "${OPERATION}" in

  "status" )
    wlst.sh "${COMMON_DIR}/wlst_app.py" status "${APP_NAME}" "" "" "" 2>&1 | grep '^~~~~~' | sed 's/~~~~~//'
  ;;

  "start" )
    f_start
    f_log_info "Starting application ${APP_NAME}"
    if [ -n "${TARGET_NAME}" ]
    then
      f_log_info "Target ${TARGET_NAME}"
    fi
    wlst.sh "${COMMON_DIR}/wlst_app.py" start "${APP_NAME}" "" "" ""
    f_finish $?
  ;;
  
  "stop" )
    f_start
    f_log_info "Stopping application ${APP_NAME}"
    if [ -n "${TARGET_NAME}" ]
    then
      f_log_info "Target ${TARGET_NAME}"
    fi
    wlst.sh "${COMMON_DIR}/wlst_app.py" stop "${APP_NAME}" "" "" ""
    f_finish $?
  ;;
  
  "deploy" )
    f_start
    f_log_info "Deploying application ${APP_NAME}"
    if [ -n "${TARGET_NAME}" ]
    then
      f_log_info "Target ${TARGET_NAME}"
    fi
    if [ -n "${APP_PATH}" ]
    then
      f_log_info "Application ${APP_PATH}"
    fi
    if [ -n "${PLAN_XML}" ]
    then
      f_log_info "Deployment plan ${PLAN_XML}"
    fi
    wlst.sh "${COMMON_DIR}/wlst_app.py" deploy "${APP_NAME}" "${TARGET_NAME}" "${APP_PATH}" "${PLAN_XML}"
    f_finish $?
  ;;
  
  "undeploy" )
    f_start
    f_log_info "Undeploy application ${APP_NAME}"
    if [ -n "${TARGET_NAME}" ]
    then
      f_log_info "Target ${TARGET_NAME}"
    fi
    wlst.sh "${COMMON_DIR}/wlst_app.py" undeploy "${APP_NAME}" "${TARGET_NAME}" "" ""
    f_finish $?
  ;;
  
  * )
    echo "ERROR!!! Invalid operation \"${OPERATION}\""
    echo "Available operations: status, start, stop, deploy, undeploy"
    exit 3
  ;;
esac
