#!/bin/bash

umask 0027

FILE_TIMESTAMP="$(date '+%Y_%m_%d')"
START_EPOCH_MILI="$(date '+%s%3N')"

COMMON_DIR="$(readlink -f $(dirname "${0}"))"

export PATH="${JAVA_HOME}/bin:${ORACLE_HOME}/oracle_common/bin:${ORACLE_HOME}/oracle_common/common/bin:${ORACLE_HOME}/OPatch:${PATH}"

export JAVA_TOOL_OPTIONS="-Dpython.path=${COMMON_DIR} -Dweblogic.security.SSL.hostnameVerifier=weblogic.security.utils.SSLWLSWildcardHostnameVerifier -Dweblogic.security.TrustKeyStore=CustomTrust -Dweblogic.security.CustomTrustKeyStoreFileName=${TRUST_STORE} -Dweblogic.security.TrustKeystoreType=${TRUST_STORE_TYPE} ${JAVA_TOOL_OPTIONS}"

if [ -z "${LOG_DIR}" ]
then
  LOG_DIR="${COMMON_DIR}/logs"
fi
LOG_FILE="${LOG_DIR}/scripts/${SCRIPT_NAME}-${FILE_TIMESTAMP}${LOG_POSTFIX}.log"
LOG_PIPE="${LOG_FILE}.tmp"

f_log() {
  echo "$(date +'%Y-%m-%d %H:%M:%S.%3N %z') $*"
}

f_log_info() {
  f_log "[INFO ] ${*} ..."
}

f_log_warn() {
  f_log "[WARN ] ${*} !"
}

f_log_err() {
  f_log "[ERROR] ${*} !!!"
}

f_duration() {
  local _start=${1}
  local _finish=${2}
  local _diff=$((${_finish} - ${_start}))
  local _milis=$((${_diff} % 1000))
  local _seconds=$((${_diff} / 1000 % 60))
  local _minutes=$((${_diff} / 1000 / 60 % 60))
  local _hours=$((${_diff} / 1000 / 60 / 60 % 24))
  local _days=$((${_diff} / 1000 / 60 / 60 / 24))
  local _text=

  if [ ${_seconds} -gt 0 ] || [ ${_milis} -gt 0 ]
  then
    _text="${_seconds}.$(printf "%03d" ${_milis})s"
  fi
  if [ ${_minutes} -gt 0 ]
  then
    _text="${_minutes}m ${_text}"
  fi
  if [ ${_hours} -gt 0 ]
  then
    _text="${_hours}h ${_text}"
  fi
  if [ ${_days} -gt 0 ]
  then
    _text="${_days}d ${_text}"
  fi
  echo "${_text}" | awk '{$1=$1};1'
}

f_check_variables() {
  for _var in ${1}
  do
    if [ -z "$(printenv "${_var}")" ]
    then
      f_log_err "Missing value for variable ${_var}"
      exit 2
    fi
  done
}

f_finish() {
  local _exit_code=0
  if [ "${1}" = "X" ]
  then
    f_log_warn "Ctrl+C pressed or SIGINT received"
    _exit_code=99
  elif [ -n "${1}" ]
  then
    _exit_code=${1}
  fi
  f_log_info "FINISHED ($(f_duration ${START_EPOCH_MILI} $(date +%s%3N)))"
  f_log_info "Log file ${LOG_FILE}"
  rm -f "${LOG_PIPE}"
  exit ${_exit_code}
}

f_start() {
  trap "f_finish X" SIGINT
  rm -f "${LOG_PIPE}" > /dev/null 2>&1
  mkdir -p "${LOG_DIR}" > /dev/null 2>&1
  mkfifo "${LOG_PIPE}"
  tee -a "${LOG_FILE}" < "${LOG_PIPE}" &
  exec > "${LOG_PIPE}"
  
  f_log_info "STARTED"
  f_log_info "Log file ${LOG_FILE}"
  START_EPOCH_MILI="$(date +%s%3N)"
}

if [ "$(whoami)" != "$(ls -l "${0}" | awk '{print $3}')" ]
then
  f_log_err "ERROR!!! Not owner."
  exit 1
fi
