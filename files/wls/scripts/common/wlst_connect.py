import os
from wlstModule import *

NM_CONF_FILE   = '%s/config/security/%s-nm.conf.props' % (os.environ['DOMAIN_HOME'], os.environ['DOMAIN_NAME'])
NM_KEY_FILE    = '%s/config/security/%s-nm.key.props' % (os.environ['DOMAIN_HOME'], os.environ['DOMAIN_NAME'])
NM_HOST        = os.environ['NODEMANAGER_HOSTNAME']
NM_PORT        = os.environ['NODEMANAGER_PORT']
NM_DOMAIN_NAME = os.environ['DOMAIN_NAME']
NM_TYPE        = os.environ['NODEMANAGER_TYPE']

AS_CONF_FILE = '%s/config/security/%s-as.conf.props' % (os.environ['DOMAIN_HOME'], os.environ['DOMAIN_NAME'])
AS_KEY_FILE  = '%s/config/security/%s-as.key.props' % (os.environ['DOMAIN_HOME'], os.environ['DOMAIN_NAME'])
AS_URL       = '%s://%s:%s' % (os.environ['ADMINSERVER_PROTOCOL'], os.environ['ADMINSERVER_HOSTNAME'], os.environ['ADMINSERVER_PORT'])


nm_connected = False;
as_connected = False;

def nm_connect():
  global nm_connected
  if not nm_connected:
    nmConnect(userConfigFile=NM_CONF_FILE, userKeyFile=NM_KEY_FILE, host=NM_HOST, port=int(NM_PORT), domainName=NM_DOMAIN_NAME, nmType=NM_TYPE)
    nm_connected = True

def nm_disconnect():
  global nm_connected
  if nm_connected:
    nmDisconnect()
    nm_connected = False


def as_connect():
  global as_connected
  if not as_connected:
    connect(userConfigFile=AS_CONF_FILE, userKeyFile=AS_KEY_FILE, url=AS_URL)
    as_connected = True

def as_disconnect():
  global as_connected
  if as_connected:
    disconnect()
    as_connected = False
