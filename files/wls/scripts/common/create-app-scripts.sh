#!/bin/bash

APP_DIR_NAME="${1}"
TARGET_NAME="${2}"

if [ -z "${APP_DIR_NAME}" ]
then
  echo 'ERROR!!! No app directory name'
  exit 1
fi

POSTFIX=
if [ -n "${TARGET_NAME}" ]
then
  POSTFIX="___${TARGET_NAME}"
fi

cat <<EOF > "start-app___${APP_DIR_NAME}"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi
export SCRIPT_NAME="\$(basename "\${0}" | awk -F'___' '{print \$1}')"

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
APP_DIR_NAME="\$(basename "\${0}" | awk -F'___' '{print \$2}')"
TARGET_NAME="\$(basename "\${0}" | awk -F'___' '{print \$3}')"
APP_DIR="\${DEPLOYMENT_DIR}/\${APP_DIR_NAME}"
APP_NAME_PATH="\$("\${COMMON_SCRIPT_DIR}/get-app-name-path.sh" "\${APP_DIR}")"
APP_NAME="\$(echo "\${APP_NAME_PATH}" | awk -F'___' '{print \$1}')"

"\${COMMON_SCRIPT_DIR}/wls-app.sh" start "\${APP_NAME}" "" "" ""
EOF
chmod u+x "start-app___${APP_DIR_NAME}"

cat <<EOF > "stop-app___${APP_DIR_NAME}"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi
export SCRIPT_NAME="\$(basename "\${0}" | awk -F'___' '{print \$1}')"

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
APP_DIR_NAME="\$(basename "\${0}" | awk -F'___' '{print \$2}')"
TARGET_NAME="\$(basename "\${0}" | awk -F'___' '{print \$3}')"
APP_DIR="\${DEPLOYMENT_DIR}/\${APP_DIR_NAME}"
APP_NAME_PATH="\$("\${COMMON_SCRIPT_DIR}/get-app-name-path.sh" "\${APP_DIR}")"
APP_NAME="\$(echo "\${APP_NAME_PATH}" | awk -F'___' '{print \$1}')"

"\${COMMON_SCRIPT_DIR}/wls-app.sh" stop "\${APP_NAME}" "" "" ""
EOF
chmod u+x "stop-app___${APP_DIR_NAME}"

cat <<EOF > "deploy-app___${APP_DIR_NAME}${POSTFIX}"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi
export SCRIPT_NAME="\$(basename "\${0}" | awk -F'___' '{print \$1}')"

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
APP_DIR_NAME="\$(basename "\${0}" | awk -F'___' '{print \$2}')"
TARGET_NAME="\$(basename "\${0}" | awk -F'___' '{print \$3}')"
APP_DIR="\${DEPLOYMENT_DIR}/\${APP_DIR_NAME}"
APP_NAME_PATH="\$("\${COMMON_SCRIPT_DIR}/get-app-name-path.sh" "\${APP_DIR}")"
APP_NAME="\$(echo "\${APP_NAME_PATH}" | awk -F'___' '{print \$1}')"
APP_PATH="\$(echo "\${APP_NAME_PATH}" | awk -F'___' '{print \$2}')"

PLAN_XML="\${DEPLOYMENT_DIR}/\${APP_NAME}/deployment-plan.xml"
if [ ! -f "\${PLAN_XML}" ]
then
  PLAN_XML=
fi

"\${COMMON_SCRIPT_DIR}/wls-app.sh" deploy "\${APP_NAME}" "\${TARGET_NAME}" "\${APP_PATH}" "\${PLAN_XML}"
EOF
chmod u+x "deploy-app___${APP_DIR_NAME}${POSTFIX}"

cat <<EOF > "undeploy-app___${APP_DIR_NAME}${POSTFIX}"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi
export SCRIPT_NAME="\$(basename "\${0}" | awk -F'___' '{print \$1}')"

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
APP_DIR_NAME="\$(basename "\${0}" | awk -F'___' '{print \$2}')"
TARGET_NAME="\$(basename "\${0}" | awk -F'___' '{print \$3}')"
APP_DIR="\${DEPLOYMENT_DIR}/\${APP_DIR_NAME}"
APP_NAME_PATH="\$("\${COMMON_SCRIPT_DIR}/get-app-name-path.sh" "\${APP_DIR}")"
APP_NAME="\$(echo "\${APP_NAME_PATH}" | awk -F'___' '{print \$1}')"

"\${COMMON_SCRIPT_DIR}/wls-app.sh" undeploy "\${APP_NAME}" "\${TARGET_NAME}" "" ""
EOF
chmod u+x "undeploy-app___${APP_DIR_NAME}${POSTFIX}"

cat <<EOF > "status-all-apps"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"

APP_NAMES=
for START_SCRIPT in \$(find "\${ADMIN_SCRIPT_DIR}" -mindepth 1 -maxdepth 1 -type f -name "start-app___*")
do
  APP_DIR_NAME="\$(basename "\${START_SCRIPT}" | awk -F'___' '{print \$2}')"
  APP_DIR="\${DEPLOYMENT_DIR}/\${APP_DIR_NAME}"
  APP_NAME_PATH="\$("\${COMMON_SCRIPT_DIR}/get-app-name-path.sh" "\${APP_DIR}")"
  APP_NAMES="\${APP_NAMES} \$(echo "\${APP_NAME_PATH}" | awk -F'___' '{print \$1}')"
done

"\${COMMON_SCRIPT_DIR}/wls-app.sh" status "\${APP_NAMES}" ""
EOF
chmod u+x "status-all-apps"
