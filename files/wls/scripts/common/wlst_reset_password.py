import os
import sys
from wlst_connect import *

DOMAIN_HOME = os.environ['DOMAIN_HOME']
USERNAME = sys.argv[1]
PASSWORD = sys.argv[2]

as_connect()
cd('/SecurityConfiguration/' + DOMAIN_HOME + '/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
cmo.resetUserPassword(USERNAME, PASSWORD)
as_disconnect()
exit()
