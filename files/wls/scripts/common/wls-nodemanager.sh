#!/bin/bash

OPERATION="${1}"
SCRIPT_NAME="${SCRIPT_NAME}_${OPERATION}"

source "$(dirname "${0}")/common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading common.sh"
  exit 1
fi

f_nm_status() {
  local _pid_file="${DOMAIN_HOME}/nodemanager/nodemanager.process.id"
  local _properties_file="${DOMAIN_HOME}/nodemanager/nodemanager.properties"
  local _listen_port=$(grep "^ListenPort" "${_properties_file}" | awk -F"=" '{print $2}')
  local _status="SHUTDOWN"

  if [ -f "${_pid_file}" ]
  then
    local _pid=$(cat "${_pid_file}")
    ps -p ${_pid} 1> /dev/null 2>&1
    local _exit_code=$?
    if [ ${_exit_code} -eq 0 ]
    then
       netstat -an | grep "LISTEN" | grep ":$LISTEN_PORT" 1> /dev/null 2>&1
       _exit_code=$?
       if [ ${_exit_code} -eq 0 ]
       then
         _status="RUNNING"
       fi
    fi
  fi
  echo "${_status}"
}

case "${OPERATION}" in

  "status" )
    f_nm_status
  ;;
  
  "start" )
    f_start
    f_log_info "Starting Node Manager"

    RETRY_MAX=50
    RETRY_WAIT=3
    RETRY_COUNT=0

    STATUS="$(f_nm_status)"
    if [ "${STATUS}" = "RUNNING" ]
    then
      f_log_warn "Skip. Node Manager is ${STATUS}."
    else
      LOG_FILE="${LOG_DIR}/nodemanager-${FILE_TIMESTAMP}.log"
      nohup "${DOMAIN_HOME}/bin/startNodeManager.sh" 1>> "${LOG_FILE}" 2>&1 &
      
      STATUS="$(f_nm_status)"
      while [ "${STATUS}" != "RUNNING" ] && [ ${RETRY_COUNT} -le ${RETRY_MAX} ]
      do
        ((RETRY_COUNT=${RETRY_COUNT}+1))
        sleep ${RETRY_WAIT}
        f_log_info ''
        
        STATUS="$(f_nm_status)"
        if [ "${STATUS}" = "RUNNING" ]
        then
          f_log_info "Node Manager is RUNNING"
          break
        fi
      done
      
      if [ "${STATUS}" != "RUNNING" ]
      then
        f_log_err "Reached maximum waiting time. Node Manager is NOT RUNNING"
      fi
    fi

    f_finish
  ;;
  
  "stop" )
    f_start
    f_log_info "Stopping Node Manager"
    "${DOMAIN_HOME}/bin/stopNodeManager.sh"
    f_finish $?
  ;;
  
  * )
    echo "ERROR!!! Invalid operation \"${OPERATION}\""
    echo "Available operations: status, start, stop"
    exit 3
  ;;
esac
