#!/bin/bash

SERVER_NAME="${1}"

if [ -z "${SERVER_NAME}" ]
then
  echo 'ERROR!!! No server name'
  exit 1
fi

cat <<EOF > "start-server___${SERVER_NAME}"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi

export SCRIPT_NAME="\$(basename "\${0}" | awk -F'___' '{print \$1}')"

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
SERVER_NAME="\$(basename "\${0}" | awk -F'___' '{print \$2}')"

"\${COMMON_SCRIPT_DIR}/wls-server.sh" start "\${SERVER_NAME}"
EOF
chmod u+x "start-server___${SERVER_NAME}"

cat <<EOF > "stop-server___${SERVER_NAME}"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi

export SCRIPT_NAME="\$(basename "\${0}" | awk -F'___' '{print \$1}')"

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
SERVER_NAME="\$(basename "\${0}" | awk -F'___' '{print \$2}')"

"\${COMMON_SCRIPT_DIR}/wls-server.sh" stop "\${SERVER_NAME}"
EOF
chmod u+x "stop-server___${SERVER_NAME}"

cat <<EOF > "kill-server___${SERVER_NAME}"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi

export SCRIPT_NAME="\$(basename "\${0}" | awk -F'___' '{print \$1}')"

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
SERVER_NAME="\$(basename "\${0}" | awk -F'___' '{print \$2}')"

"\${COMMON_SCRIPT_DIR}/wls-server.sh" kill "\${SERVER_NAME}"
EOF
chmod u+x "kill-server___${SERVER_NAME}"

cat <<EOF > "cleanup-server___${SERVER_NAME}"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi

export SCRIPT_NAME="\$(basename "\${0}" | awk -F'___' '{print \$1}')"

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"
SERVER_NAME="\$(basename "\${0}" | awk -F'___' '{print \$2}')"

"\${COMMON_SCRIPT_DIR}/wls-server.sh" cleanup "\${SERVER_NAME}"
EOF
chmod u+x "cleanup-server___${SERVER_NAME}"

cat <<EOF > "status-all-servers"
#!/bin/bash

source "\$(dirname "\${0}")/domain.env"
if [ \${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading domain.env"
  exit 1
fi

export SCRIPT_NAME="\$(basename "\${0}" | awk -F'___' '{print \$1}')"

COMMON_SCRIPT_DIR="\$(readlink -f "\${ADMIN_SCRIPT_DIR}/../")/common"

SERVER_NAMES=
for START_SCRIPT in \$(find "\${ADMIN_SCRIPT_DIR}" -mindepth 1 -maxdepth 1 -type f -name "start-server___*")
do
  SERVER_NAMES="\${SERVER_NAMES} \$(echo "\$(basename \${START_SCRIPT} | awk -F'___' '{print \$2}')")"
done

"\${COMMON_SCRIPT_DIR}/wls-server.sh" status "\${SERVER_NAMES}"
EOF
chmod u+x "status-all-servers"
