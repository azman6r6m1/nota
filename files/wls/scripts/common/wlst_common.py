import sys

def get_max_length(items):
  max_length = 0
  for item in items:
    if len(item) > max_length:
      max_length = len(item)
