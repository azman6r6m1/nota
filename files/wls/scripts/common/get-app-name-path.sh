#!/bin/bash

APP_DIR="${1}"

APP_PATH="${APP_DIR}"
APP_NAME="$(basename "${APP_DIR}")"

APP_ARCHIVE="$(find "${APP_DIR}" -mindepth 1 -maxdepth 1 -type f -name "*.ear" -or -name "*.war" -or -name "*.jar")"
if [ -n "${APP_ARCHIVE}" ]
then
  APP_PATH="${APP_ARCHIVE}"
  APP_NAME="$(basename "${APP_ARCHIVE}" | awk -F'.' '{for(i=1;i<NF-1;i++) printf $i"."; printf $(NF-1)}')"
fi

echo "${APP_NAME}___${APP_PATH}"
