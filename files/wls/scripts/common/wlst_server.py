import sys
from wlst_connect import *

OPERATION = sys.argv[1].strip().lower()
SERVER_NAME = sys.argv[2].strip()

SHUTDOWN_TIMEOUT = 300

def print_server_status(entries):
  name_header = 'Server'
  status_header = 'Status'

  name_max_length = len(name_header)
  status_max_length = len(status_header)
  for entry in entries:
    if len(entry[0]) > name_max_length:
      name_max_length = len(entry[0])

    if len(entry[1]) > status_max_length:
      status_max_length = len(entry[1])

  print '~~~~~%s---%s' % ('-' * name_max_length, '-' * status_max_length)
  print '~~~~~%-*s   %-*s' % (name_max_length, name_header, status_max_length, status_header)
  print '~~~~~%s---%s' % ('-' * name_max_length, '-' * status_max_length)
  for entry in entries:
    print '~~~~~%-*s   %-*s' % (name_max_length, entry[0], status_max_length, entry[1])
  print '~~~~~%s---%s' % ('-' * name_max_length, '-' * status_max_length)


def show_server_status():
  entries = []
  server_names = SERVER_NAME.split(' ')

  nm_connect()

  for server_name in server_names:
    status = nmServerStatus(server_name)
    entries.append([server_name, status])

  nm_disconnect()

  print_server_status(entries)


def start_server():
  nm_connect()

  nmStart(SERVER_NAME)

  nm_disconnect()


def stop_server():
  as_connect()

  shutdown(name=SERVER_NAME, ignoreSessions='true', force='true', block='true', timeOut=SHUTDOWN_TIMEOUT)

  as_disconnect()


def kill_server():
  nm_connect()

  nmKill(SERVER_NAME)

  nm_disconnect()


if OPERATION == 'start':
  start_server()

elif OPERATION == 'stop':
  stop_server()

elif OPERATION == 'kill':
  kill_server()

elif OPERATION == 'status':
  show_server_status()

else:
  print 'ERROR!!! Invalid operation "%s"' % OPERATION

exit()
