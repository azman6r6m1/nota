#!/bin/bash

# By Azman Kudus
# Last update on 17 Jan 2022

CURRENT_USER="$(whoami)"
SCRIPT_OWNER="$(ls -l "${0}" | cut -d' ' -f3)"
SCRIPT_DIR="$(readlink -f $(dirname "${0}"))"
SCRIPT_NAME="$(basename "${0}")"

if [ "${CURRENT_USER}" != "${SCRIPT_OWNER}" ]
then
  log_err "You are not script owner. Login as \"${SCRIPT_OWNER}\" and try again."
  exit 1
fi

umask 027

log() {
  echo "$(date +'%Y-%m-%d %H:%M:%S.%3N %z') $*"
}

log_info() {
  log "[INFO ] ${*} ..."
}

log_warn() {
  log "[WARN ] ${*} !"
}

log_err() {
  log "[ERROR] ${*} !!!"
  log "Run \"bash ${SCRIPT_NAME} -h\" to get more help"
}

PATCH_DIR="${SCRIPT_DIR}/patch"
PATCH_TMP_DIR="${PATCH_DIR}/tmp"

print_help() {
  cat <<EOF
1. Place SPB patch zip file and/or individual patch zip files into "patch" folder.
2. Ensure the following patch names with a prefix.
   Stack Patch Bundle (SPB) zip file prefix is "wls-spb-".
   OPatch zip file prefix is "opatch-".
3. Export ORACLE_BASE to the desired path. Use same path for all scripts.
4. This script will patch \$ORACLE_BASE/home. Backup if required.
5. Run this script using Bash (bash ${SCRIPT_NAME}). Better to run with nohup.
EOF
}

upgrade_opatch() {
  OPATCH_ZIP="${1}"

  if [ -z "${OPATCH_ZIP}" ]
  then
    log_warn "Missing OPatch zip file"
    return
  fi
  log_info "OPatch zip file found. ${OPATCH_ZIP}"
  
  cd "${PATCH_TMP_DIR}"
  
  OPATCH_DIR="$(zipinfo "${OPATCH_ZIP}" | grep ^d | head -1 | awk '{print $NF}' | sed 's/\/$//')"
  OPATCH_JAR="$(zipinfo "${OPATCH_ZIP}" | grep "${OPATCH_DIR}/opatch_generic.jar" | awk '{print $NF}')"
  if [ -z "${OPATCH_JAR}" ]
  then
    log_warn "Missing OPatch jar file"
    return
  fi
  log_info "OPatch jar found. ${OPATCH_JAR}"
  log_info "OPatch version to be upgraded is $(unzip -cqq "${OPATCH_ZIP}" "${OPATCH_DIR}/version.txt")"
  
  log_info "Extracting ${OPATCH_JAR} from ${OPATCH_ZIP}"
  unzip -oqq "${OPATCH_ZIP}" "${OPATCH_JAR}"
  
  log_info "Before OPatch upgrade: $(opatch version | head -1)"
  
  log_info "Upgrading OPatch"
  java -jar "${OPATCH_JAR}" -silent "oracle_home=${ORACLE_HOME}" -invPtrLoc "${ORACLE_HOME}/oraInst.loc"
  
  log_info "After OPatch upgrade: $(opatch version | head -1)"
  cd "${PATCH_DIR}"
} 

if [ "$1" = "--help" ] || [ "$1" = "-h" ]
then
  print_help
  exit 0
fi

if [ -z "${ORACLE_BASE}" ]
then
  log_err "ORACLE_BASE is not set"
  exit 2
fi

mkdir -p "${ORACLE_BASE}"

SPB_PATCH_LIST="linux64_patchlist.txt"
INDIV_PATCH_LIST="linux64_patchlist_indiv.txt"

log_info "Starting WebLogic Server SPB patch"
log_info "ORACLE_BASE is set to ${ORACLE_BASE}"

export JAVA_HOME="${ORACLE_BASE}/jdk"
export PATH="${JAVA_HOME}/bin:${PATH}"
export _JAVA_OPTIONS="-Djava.io.tmpdir=${PATCH_TMP_DIR}"
log_info "JAVA_HOME is set to ${JAVA_HOME}"
log_info "$(java -version 2>&1 | grep 'java version ')"

export ORACLE_HOME="${ORACLE_BASE}/home"
export PATH="${ORACLE_HOME}/OPatch:${PATH}"
log_info "ORACLE_HOME is set to ${ORACLE_HOME}"
log_info "$(opatch version | head -1)"

cd "${PATCH_DIR}"

SPB_ZIP="$(find "${PATCH_DIR}" -type f -name "wls-spb-p*_Generic.zip" | head -1)"
if [ -z "${SPB_ZIP}" ]
then
  log_err "Missing SPB patch zip file"
  exit 3
fi
log_info "SPB patch zip file found. $SPB_ZIP"

SPB_DIR="$(zipinfo "${SPB_ZIP}" | grep -P '^d.*' | head -1 | grep -P 'WLS_SPB_.*' | awk '{print $NF}' | sed 's/\/$//')"
if [ -z "${SPB_DIR}" ]
then
  log_err "Missing SPB patch folder within the zip file"
  exit 4
fi
log_info "SPB patch folder found. ${SPB_DIR}"

log_info "Create temporary directory"
mkdir "${PATCH_TMP_DIR}"
cd "${PATCH_TMP_DIR}"

log_info "Extracting everything from ${SPB_ZIP}"
unzip -oqq "${SPB_ZIP}"

SPB_DIR="${PATCH_TMP_DIR}/${SPB_DIR}"

OPATCH_ZIP_PATH="$(find "${SPB_DIR}/tools/opatch/generic" -type f -name 'p*_Generic.zip' | head -1)"
log_info "Upgrade OPatch from SPB zip"
upgrade_opatch "${OPATCH_ZIP_PATH}"

cd "${SPB_DIR}/binary_patches"

log_info "SPB patch to be applied"
cat "${SPB_PATCH_LIST}"
log_info "Total $(cat "${SPB_PATCH_LIST}" | wc -l) patches"

log_info "Patching SPB"
opatch napply -silent -phBaseFile "${SPB_PATCH_LIST}" -oh "${ORACLE_HOME}" -invPtrLoc "{$ORACLE_HOME}/oraInst.loc"

cd "${PATCH_DIR}"
OPATCH_ZIP_PATH="$(find "${PATCH_DIR}" -type f -name 'opatch-p*_Generic.zip' | head -1)"
log_info "Upgrade OPatch from individual patch zip"
upgrade_opatch "${OPATCH_ZIP_PATH}"

log_info "Individual patches to be applied"
ls "${PATCH_DIR}" | grep -P '.zip$' | grep -vP '^(opatch|wls-spb)' > "${INDIV_PATCH_LIST}"
cat "${INDIV_PATCH_LIST}"
log_info "Total $(cat "${INDIV_PATCH_LIST}" | wc -l) patches"

log_info "Patching"
opatch napply -silent -phBaseFile "${INDIV_PATCH_LIST}" -oh "${ORACLE_HOME}" -invPtrLoc "${ORACLE_HOME}/oraInst.loc"

log_info "Cleaning up temporary directory"
rm -rf "${PATCH_TMP_DIR}"

cd "${SCRIPT_DIR}"
log_info "Finished"
