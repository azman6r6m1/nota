
#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

export REQUIRE_TRUST_STORE=1
source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Disable Anonymous JNDI and RMI
Usage: bash ${0}
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "DOMAIN_HOME ADMIN_PORT"

f_start

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
from wlst_log import *

${WLST_CONNECT}

edit()
startEdit()

log_info('Disabling remote anonymous access')
cd('/SecurityConfiguration/${DOMAIN_NAME}')
set('RemoteAnonymousJNDIEnabled','false')
set('RemoteAnonymousRMIIIOPEnabled','false')
set('RemoteAnonymousRMIT3Enabled','false')

save()
activate()

disconnect()
exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Disabling failed"

f_finish