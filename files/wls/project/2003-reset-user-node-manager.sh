#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Reset node manager user
Usage: bash ${0}
EOF
}

f_start

f_backup_file "${DOMAIN_HOME}/config/nodemanager/nm_password.properties"
cat <<EOF > "${DOMAIN_HOME}/config/nodemanager/nm_password.properties"
username=${NODEMANAGER_USERNAME}
password=${NODEMANAGER_PASSWORD}
EOF

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
import os
from weblogic.security import UsernameAndPassword, UserConfigFileManager
from wlst_log import *

log_info('Reading domain ${DOMAIN_HOME}')
readDomain('${DOMAIN_HOME}')

log_info('Resetting new nodemanager credential')
cd('/SecurityConfiguration/${DOMAIN_NAME}')
set('NodeManagerUsername','${NODEMANAGER_USERNAME}')
set('NodeManagerPasswordEncrypted','${NODEMANAGER_PASSWORD}')
updateDomain()
closeDomain()

log_info('Creating new config and key files')
System.setProperty("weblogic.management.confirmKeyfileCreation", "true");
user_pass = UsernameAndPassword('${NODEMANAGER_USERNAME}', '${NODEMANAGER_PASSWORD}')
UserConfigFileManager.setUsernameAndPassword(user_pass, '${NODEMANAGER_CONF_FILE}.new', '${NODEMANAGER_KEY_FILE}.new', 'weblogic.management')
os.rename('${NODEMANAGER_CONF_FILE}.new', '${NODEMANAGER_CONF_FILE}')
os.rename('${NODEMANAGER_KEY_FILE}.new', '${NODEMANAGER_KEY_FILE}')

exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Reset user failed"

f_finish
