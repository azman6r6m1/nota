#!/bin/bash

# By Azman Kudus
# Last update on 17 Jan 2022

shopt -s expand_aliases
alias awk=gawk
alias df=gdf
alias find=gfind
alias grep=ggrep
alias sed=gsed
alias tar=gtar

CURRENT_USER="$(whoami)"
SCRIPT_OWNER="$(ls -l "${0}" | cut -d' ' -f3)"
SCRIPT_DIR="$(readlink -f $(dirname "${0}"))"
SCRIPT_NAME="$(basename "${0}")"

if [ "${CURRENT_USER}" != "${SCRIPT_OWNER}" ]
then
  log_err "You are not script owner. Login as \"${SCRIPT_OWNER}\" and try again."
  exit 1
fi

umask 027

log() {
  echo "$(date +'%Y-%m-%d %H:%M:%S.%3N %z') $*"
}

log_info() {
  log "[INFO ] ${*} ..."
}

log_warn() {
  log "[WARN ] ${*} !"
}

log_err() {
  log "[ERROR] ${*} !!!"
  log "Run \"bash ${SCRIPT_NAME} -h\" to get more help"
}

print_help() {
  echo <<EOF
1. Export ORACLE_BASE to the desired path. Use same path for all scripts.
2. This script will use JDK from $ORACLE_BASE/jdk and installation home from $ORACLE_BASE/home.
3. This script will use $ORACLE_BASE/tmp as temporary directory.
3. Run this script using Bash (bash $0). Better to run with nohup.
EOF
}

if [ "$1" = "--help" ] || [ "$1" = "-h" ]
then
  print_help
  exit 0
fi

if [ -z "${ORACLE_BASE}" ]
then
  log_err "ORACLE_BASE is not set"
  exit 2
fi

log_info "Starting WebLogic Server SPB patch"
log_info "ORACLE_BASE is set to ${ORACLE_BASE}"

export JAVA_HOME="${ORACLE_BASE}/jdk"
export PATH="${JAVA_HOME}/bin:${PATH}"
export _JAVA_OPTIONS="-Djava.io.tmpdir=${PATCH_TMP_DIR}"
log_info "JAVA_HOME is set to ${JAVA_HOME}"

export ORACLE_HOME="${ORACLE_BASE}/home"
export PATH="${ORACLE_HOME}/OPatch:${PATH}"
log_info "ORACLE_HOME is set to ${ORACLE_HOME}"

ORACLE_TMP="${ORACLE_BASE}/tmp"
log_info "Temporary directory is set to ${ORACLE_TMP}"

mkdir -p "${ORACLE_TMP}"

log "Hostname"
hostname

log "Current user and groups"
id

log "Operating System"
cat /etc/os-release | grep -P '^PRETTY_NAME='

log "Kernel"
uname -a

log "CPU"
lscpu | grep -P '^(Architecture|CPU\(s\)|CPU MHz): '

log "Memory"
lsmem | grep '^Total online memory: '

log "Swap"
swapon --show

log "Disk"
df -Th

log "Java version"
java -version 2> &1 | grep 'java version '

log "OPatch version"
opatch version | head -1

log "Installation"
grep '<distribution status="installed"' "${ORACLE_HOME}inventory/registry.xml"

log "Applied patches"
opatch lsinventory -oh "${ORACLE_HOME}" -invPtrLoc "${ORACLE_HOME}/oraInst.loc" | grep -P '^Patch '

THIRDPARTY_DIR="${ORACLE_HOME}/oracle_common/modules/thirdparty"

log "Checking Log4j version in ${THIRDPARTY_DIR}"
for JAR_FILE in $(find "${THIRDPARTY_DIR}" -type f -name "log4j*.jar")
do
  log "Checking version info of ${JAR_FILE}"
  unzip -cqq "${JAR_FILE}" "META-INF/MANIFEST.MF" | grep 'Version:'
done
