#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/0000-install.env"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 0000-install.env"
  exit 1
fi

WLST_SCRIPT="$(readlink -f "${0}").wlst.tmp"

cat <<EOF > "${WLST_SCRIPT}"
import sys
from os import path
from wlstModule import *

ENABLE_TRACEBACK = False

MODE = sys.argv[1]
TEXT = sys.argv[2]
DOMAIN_PATH = "${DOMAIN_HOME}"

service = weblogic.security.internal.SerializedSystemIni.getEncryptionService(DOMAIN_PATH)  
encryption = weblogic.security.internal.encryption.ClearOrEncryptedService(service)

if MODE[0].lower() == 'e':
  print encryption.encrypt(TEXT)

elif MODE[0].lower() == 'd':
  print encryption.decrypt(TEXT)

else:
  sys.exit(1)
EOF

wlst.sh "${WLST_SCRIPT}" "${1}" "${2}" 2>&1 | grep -v "Picked up JAVA_TOOL_OPTIONS: " | tail -1
rm -f "${WLST_SCRIPT}"
