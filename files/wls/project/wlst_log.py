# By Azman Kudus
# Last update on 17 Jan 2022

from java.time import ZonedDateTime, ZoneId
from java.time.format import DateTimeFormatter
from datetime import datetime

DATETIME_FORMATTER = DateTimeFormatter.ofPattern('YYYY-MM-dd HH:mm:ss.SSS Z')

""" Log message to STDOUT """
def log(level, message):
  print '%s [%s] %s' % (DATETIME_FORMATTER.format(ZonedDateTime.now(ZoneId.systemDefault())), level, message)

""" Log debug message """
def log_debug(message):
  log('DEBUG ', message)

""" Log info message """
def log_info(message):
  log('INFO ', message)

""" Log warning message """
def log_warn(message):
  log('WARN ', message)

""" Log error message """
def log_error(message):
  log('ERROR', message)
  traceback.print_exc()
