#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Set nodemanager properties
Usage: bash ${0}
EOF
}

f_start

NODEMANAGER_PROPERTIES="${DOMAIN_HOME}/nodemanager/nodemanager.properties"

f_log_info "Updating nodemanager properties"
f_backup_file "${NODEMANAGER_PROPERTIES}"

f_delete_append "${NODEMANAGER_PROPERTIES}" "LogFile=.*" "LogFile=${LOGS_DIR}/${DOMAIN_NAME}/NodeManager/nodemanager.log"
f_delete_append "${NODEMANAGER_PROPERTIES}" "LogToStderr=.*" "LogToStderr=false"
f_delete_append "${NODEMANAGER_PROPERTIES}" "LogAppend=.*" "LogAppend=true"
f_delete_append "${NODEMANAGER_PROPERTIES}" "LogLimit=.*" "LogLimit=10485760"
f_delete_append "${NODEMANAGER_PROPERTIES}" "LogCount=.*" "LogCount=100"
f_delete_append "${NODEMANAGER_PROPERTIES}" "NativeVersionEnabled=.*" "NativeVersionEnabled=true"
f_delete_append "${NODEMANAGER_PROPERTIES}" "QuitEnabled=.*" "QuitEnabled=false"
f_delete_append "${NODEMANAGER_PROPERTIES}" "weblogic.StartScriptEnabled=.*" "weblogic.StartScriptEnabled=true"
f_delete_append "${NODEMANAGER_PROPERTIES}" "weblogic.StartScriptName=.*" "weblogic.StartScriptName=startWebLogic.sh"
f_delete_append "${NODEMANAGER_PROPERTIES}" "weblogic.StopScriptEnabled=.*" "weblogic.StopScriptEnabled=false"
f_delete_append "${NODEMANAGER_PROPERTIES}" "weblogic.StopScriptName=.*" ""

f_finish
