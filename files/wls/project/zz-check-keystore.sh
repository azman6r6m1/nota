#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi
f_start

f_print_help() {
  cat <<EOF
Create identity and trust stores.
The following variables must be defined in wls.env.
  1.  DOMAIN_HOME       : Domain directory
  2.  KEYSTORE_TYPE     : Keystore type
  3.  KEYSTORE_PASSWORD : Keystore and key password
  4.  IDENTITY_STORE    : Identity store for Node Manager and Admin Server
  5.  TRUST_STORE       : Trust store for Node Manager and Admin Server

Usage: bash ${0}
EOF
}

f_log_info "Verifying identity store"
keytool -list -v -keystore "${IDENTITY_STORE}" -storetype "${KEYSTORE_TYPE}" -storepass "${KEYSTORE_PASSWORD}"

f_log_info "Verifying trust store"
keytool -list -v -keystore "${TRUST_STORE}" -storetype "${KEYSTORE_TYPE}" -storepass "${KEYSTORE_PASSWORD}"

f_finish
