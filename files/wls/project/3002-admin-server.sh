#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

export REQUIRE_TRUST_STORE=1
source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Start, stop, restart and check status of Oracle WebLogic Admin Server.
The following variables must be defined in wls.env.
  1. DOMAIN_HOME : Domain directory
  2. LOGS_DIR    : Base logs directory

Usage: bash ${0}
EOF
}

f_as_status() {
  local _status="$(
  wlst.sh <<EOF 2>&1 | grep -P "^Successfully connected to Admin Server"
connect('${ADMINSERVER_USERNAME}', '${ADMINSERVER_PASSWORD}', 't3://${ADMINSERVER_HOSTNAME}:${ADMINSERVER_PORT}')
disconnect()
exit()
EOF
)"
  if [ -z "${_status}" ]
  then
    echo "SHUTDOWN"
  else
    echo "RUNNING"
  fi
}

f_as_start() {
  local _status="$(f_as_status)"
  if [ "${_status}" = "RUNNING" ]
  then
    f_log_warn "Skip. Admin Server is ${_status}."
  else
    local _log_file="${LOG_DIR}/adminserver.log"
    nohup "${DOMAIN_HOME}/bin/startWebLogic.sh" 1>> $_log_file 2>&1 &
    
    local _retry_max=50
    local _retry_wait=3
    local _retry_count=0
    
    local _status="$(f_as_status)"
    while [ "${_status}" != "RUNNING" ] && [ ${_retry_count} -le ${_retry_max} ]
    do
      ((_retry_count=${_retry_count}+1))
      sleep ${_retry_wait}
      f_log_info ''
      
      _status="$(f_as_status)"
      if [ "${_status}" = "RUNNING" ]
      then
        f_log_info "Admin Server is RUNNING"
        break
      fi
    done
    
    if [ "${_status}" != "RUNNING" ]
    then
      f_log_err "Reached maximum waiting time. Admin Server is NOT RUNNING"
    fi
  fi
}

f_as_stop() {
  local _status="$(f_as_status)"
  if [ "${_status}" != "RUNNING" ]
  then
    f_log_warn "Skip. Admin Server is ${_status}."
  else
    "${DOMAIN_HOME}/bin/stopWebLogic.sh"
  fi
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "DOMAIN_NAME LOGS_DIR"

case "${1}" in
    "status" )
      f_as_status
    ;;
    
    "start" )
      f_start
      f_log_info "Starting Admin Server"
      f_as_start
      f_finish
    ;;
    
    "stop" )
      f_start
      f_log_info "Stopping Admin Server"
      f_as_stop
      f_finish
    ;;
    
    "restart" )
      f_start
      f_log_info "Stopping Admin Server"
      f_as_stop
      f_log_info "Starting Admin Server"
      f_as_start
      f_finish
    ;;
    
    * )
      echo "ERROR!!! Invalid option"
    ;;
esac
