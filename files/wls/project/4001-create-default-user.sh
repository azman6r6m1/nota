#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

export REQUIRE_TRUST_STORE=1
source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Create default Oracle WebLogic Server users
Usage: bash ${0}
EOF
}

f_start

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
from wlst_log import *

${WLST_CONNECT}

log_info('Creating LCMUser')
cd('/SecurityConfiguration/${DOMAIN_NAME}/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
cmo = cd(pwd())
cmo.createUser('LCMUser', '${ADMINSERVER_PASSWORD}', 'This is the default service account for WebLogic Server Lifecycle Manager configuration updates.')
cmo.addMemberToGroup('Administrators', 'LCMUser')

log_info('Creating OracleSystemUser')
cmo.createUser('OracleSystemUser', '${ADMINSERVER_PASSWORD}', 'Oracle application software system user.')
cmo.createGroup('OracleSystemGroup', 'Oracle application software system group.')
cmo.addMemberToGroup('OracleSystemGroup', 'OracleSystemUser')

log_info('Creating secondary administrator')
cmo.createUser('${ADMIN_USERNAME}', '${ADMIN_PASSWORD}', 'This is administrator account for interactive operations.')
cmo.addMemberToGroup('Administrators', '${ADMIN_USERNAME}')

disconnect()
exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Create user failed"

f_finish
