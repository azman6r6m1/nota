#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

COMMON_NAME="${1}"
DOMAIN_LIST="${2}"

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Create identity and trust stores.
Usage: bash ${0} <common_name> <domain_list>
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

if [ -z "${COMMON_NAME}" ]
then
  f_log_err "No common name"
  exit 1
fi

f_check_variables "DOMAIN_HOME KEYSTORE_TYPE KEYSTORE_PASSWORD IDENTITY_STORE TRUST_STORE SSL_VALID_DAYS SSL_KEY_ALG SSL_SIGN_ALG SSL_COUNTRY_CODE SSL_STATE_NAME SSL_CITY_NAME SSL_ORGANIZATION_NAME SSL_ORGANIZATION_UNIT_NAME SSL_EMAIL_ADDRESS"

f_start

KEYSTORE_DIR="$(readlink -f "$(dirname "${IDENTITY_STORE}")")"
TRUSTED_CERT="${KEYSTORE_DIR}/trusted.crt"

DISTINGUISHED_NAME="CN=${COMMON_NAME}, C=${SSL_COUNTRY_CODE}, ST=${SSL_STATE_NAME}, L=${SSL_CITY_NAME}, O=${SSL_ORGANIZATION_NAME}, OU=${SSL_ORGANIZATION_UNIT_NAME}, EMAILADDRESS=${SSL_EMAIL_ADDRESS}"
DNS_SAN="san=dns:${COMMON_NAME}"
for DOMAIN in ${DOMAIN_LIST}
do
  DNS_SAN="${DNS_SAN},dns:${DOMAIN}"
done

f_log_info "Creating identity stores"
CMD="keytool -genkeypair -alias \"${COMMON_NAME}\" ${SSL_KEY_ALG} -sigalg ${SSL_SIGN_ALG} -validity ${SSL_VALID_DAYS} -keystore \"${IDENTITY_STORE}\" -storetype \"${KEYSTORE_TYPE}\" -storepass \"${KEYSTORE_PASSWORD}\" -keypass \"${KEYSTORE_PASSWORD}\" -dname \"${DISTINGUISHED_NAME}\" -ext \"eku=serverAuth\" -ext \"ku=digitalSignature,keyEncipherment\" -ext \"${DNS_SAN}\""
eval "${CMD}"
f_error_exit ${PIPESTATUS[0]} "Create identity store failed"
rm -f "${WLST_SCRIPT}"

f_log_info "Creating trust stores"
keytool -export -keystore "${IDENTITY_STORE}" -storetype "${KEYSTORE_TYPE}" -storepass "${KEYSTORE_PASSWORD}" -alias "${COMMON_NAME}" -file "${TRUSTED_CERT}"

keytool -import -trustcacerts -keystore "${TRUST_STORE}" -storetype "${KEYSTORE_TYPE}" -storepass "${KEYSTORE_PASSWORD}" -alias "${COMMON_NAME}" -file "${TRUSTED_CERT}" -noprompt
CMD_STATUS=${PIPESTATUS[0]}
rm -f "${TRUSTED_CERT}"
f_error_exit ${CMD_STATUS} "Create trustore failed"

f_log_info "Create keystore password files"
bash "${SCRIPT_DIR}/9001-encdec.sh" e "${KEYSTORE_PASSWORD}" > "${IDENTITY_STORE}.pass"
bash "${SCRIPT_DIR}/9001-encdec.sh" e "${KEYSTORE_PASSWORD}" > "${TRUST_STORE}.pass"

f_finish
