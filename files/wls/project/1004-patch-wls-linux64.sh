#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi
f_start

PATCH_DIR="${SCRIPT_DIR}/patch"
PATCH_TMP_DIR="${PATCH_DIR}/tmp"
export _JAVA_OPTIONS="-Djava.io.tmpdir=${PATCH_TMP_DIR}"

f_print_help() {
  cat <<EOF
Apply security patches on Oracle WebLogic. Place patches zip file into ${SCRIPT_DIR}/wls directory.
Ensure the following patch names with a prefix.
   Stack Patch Bundle (SPB) zip file prefix is "wls-spb-".
   OPatch zip file prefix is "opatch-".
  
Usage: bash ${0}
EOF
}

f_upgrade_opatch() {
  OPATCH_ZIP="${1}"

  if [ -z "${OPATCH_ZIP}" ]
  then
    f_log_warn "Missing OPatch zip file"
    return
  fi
  f_log_info "OPatch zip file found. ${OPATCH_ZIP}"

  cd "${PATCH_TMP_DIR}"

  OPATCH_DIR="$(zipinfo "${OPATCH_ZIP}" | grep ^d | head -1 | awk '{print $NF}' | sed 's/\/$//')"
  OPATCH_JAR="$(zipinfo "${OPATCH_ZIP}" | grep "${OPATCH_DIR}/opatch_generic.jar" | awk '{print $NF}')"
  if [ -z "${OPATCH_JAR}" ]
  then
    f_log_warn "Missing OPatch jar file"
    return
  fi
  f_log_info "OPatch jar found. ${OPATCH_JAR}"
  f_log_info "OPatch version to be upgraded is $(unzip -cqq "${OPATCH_ZIP}" "${OPATCH_DIR}/version.txt")"

  f_log_info "Extracting ${OPATCH_JAR} from ${OPATCH_ZIP}"
  unzip -oqq "${OPATCH_ZIP}" "${OPATCH_JAR}"

  f_log_info "Before OPatch upgrade: $(opatch version | head -1)"

  f_log_info "Upgrading OPatch"
  java -jar "${OPATCH_JAR}" -silent "oracle_home=${ORACLE_HOME}" -invPtrLoc "${ORACLE_HOME}/oraInst.loc"

  f_log_info "After OPatch upgrade: $(opatch version | head -1)"
  cd "${PATCH_DIR}"
}

if [ "$1" = "--help" ] || [ "$1" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "ORACLE_HOME"

SPB_PATCH_LIST="linux64_patchlist.txt"
INDIV_PATCH_LIST="linux64_patchlist_indiv.txt"

f_log_info "ORACLE_HOME is set to ${ORACLE_HOME}"
f_log_info "$(opatch version | head -1)"

cd "${PATCH_DIR}"

SPB_ZIP="$(find "${PATCH_DIR}" -type f -name "wls-spb-p*_Generic.zip" | head -1)"
if [ -z "${SPB_ZIP}" ]
then
  f_log_warn "Missing SPB patch zip file"
else
  f_log_info "SPB patch zip file found. $SPB_ZIP"

  SPB_DIR="$(zipinfo "${SPB_ZIP}" | grep -P '^d.*' | head -1 | grep -P 'WLS_SPB_.*' | awk '{print $NF}' | sed 's/\/$//')"
  if [ -z "${SPB_DIR}" ]
  then
    f_log_err "Missing SPB patch folder within the zip file"
    exit 4
  fi
  f_log_info "SPB patch folder found. ${SPB_DIR}"

  f_log_info "Create temporary directory"
  mkdir "${PATCH_TMP_DIR}"
  cd "${PATCH_TMP_DIR}"

  f_log_info "Extracting everything from ${SPB_ZIP}"
  unzip -oqq "${SPB_ZIP}"

  SPB_DIR="${PATCH_TMP_DIR}/${SPB_DIR}"

  OPATCH_ZIP_PATH="$(find "${SPB_DIR}/tools/opatch/generic" -type f -name 'p*_Generic.zip' | head -1)"
  f_log_info "Upgrade OPatch from SPB zip"
  f_upgrade_opatch "${OPATCH_ZIP_PATH}"

  cd "${SPB_DIR}/binary_patches"

  f_log_info "SPB patch to be applied"
  cat "${SPB_PATCH_LIST}"
  f_log_info "Total $(cat "${SPB_PATCH_LIST}" | wc -l) patches"

  f_log_info "Patching SPB"
  opatch napply -silent -phBaseFile "${SPB_PATCH_LIST}" -oh "${ORACLE_HOME}" -invPtrLoc "{$ORACLE_HOME}/oraInst.loc" 2>&1
fi

cd "${PATCH_DIR}"
OPATCH_ZIP_PATH="$(find "${PATCH_DIR}" -type f -name 'opatch-p*_Generic.zip' | head -1)"
f_log_info "Upgrade OPatch from individual patch zip"
f_upgrade_opatch "${OPATCH_ZIP_PATH}"

f_log_info "Individual patches to be applied"
ls "${PATCH_DIR}" | grep -P '.zip$' | grep -vP '^(opatch|wls-spb)' > "${INDIV_PATCH_LIST}"
cat "${INDIV_PATCH_LIST}"
f_log_info "Total $(cat "${INDIV_PATCH_LIST}" | wc -l) patches"

f_log_info "Patching"
opatch napply -silent -phBaseFile "${INDIV_PATCH_LIST}" -oh "${ORACLE_HOME}" -invPtrLoc "${ORACLE_HOME}/oraInst.loc" 2>&1

f_log_info "Cleaning up temporary directory"
rm -rf "${PATCH_TMP_DIR}"

cd "${SCRIPT_DIR}"

f_finish
