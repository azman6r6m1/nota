#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Set console extensions
Usage: bash ${0}
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "DOMAIN_HOME"

f_start

CONSOLE_DIR="${DOMAIN_HOME}/servers/AdminServer/data/console"

mkdir -p "${CONSOLE_DIR}"

f_log_info "Setting up extensions"
cat <<EOF > "${CONSOLE_DIR}/ConsoleExtensions.xml"
<?xml version='1.0' encoding='UTF-8'?>
<extensions>
  <extension-def module="console" path="weblogic-sca-console-l10n.jar" enabled="false"/>
  <extension-def module="console" path="sipserver-console-ext.jar" enabled="false" requiredServices="com.oracle.core.sip.activator"/>
  <extension-def module="console" path="coherence.jar" enabled="false"/>
  <extension-def module="console" path="diameter-console-ext.jar" enabled="false" requiredServices="com.oracle.core.sip.activator"/>
  <extension-def module="console" path="core-jms.jar" enabled="true" requiredServices="JMS"/>
  <extension-def module="console" path="spring-console.jar" enabled="false"/>
  <extension-def module="console" path="diagnostics-console-extension.jar" enabled="false"/>
  <extension-def module="console" path="weblogic-sca-console.jar" enabled="false"/>
  <extension-def module="console" path="wtc.jar" enabled="false"/>
  <extension-def module="console" path="coherence-l10n.jar" enabled="false"/>
  <extension-def module="console" path="jolt-l10n.jar" enabled="false"/>
  <extension-def module="console" path="sipserver-console-ext-l10n.jar" enabled="false" requiredServices="com.oracle.core.sip.activator"/>
  <extension-def module="console" path="spring-console-l10n.jar" enabled="false"/>
  <extension-def module="console" path="core-ejb.jar" enabled="true" requiredServices="EJB"/>
  <extension-def module="console" path="wtc-l10n.jar" enabled="false"/>
  <extension-def module="console" path="core-connector.jar" enabled="true" requiredServices="CONNECTOR"/>
  <extension-def module="console" path="jolt.jar" enabled="false"/>
  <extension-def module="console" path="core-partition-l10n.jar" enabled="false"/>
  <extension-def module="console" path="core-partition.jar" enabled="false"/>
</extensions>
EOF

f_finish
