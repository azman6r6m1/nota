#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

MACHINE_NAME="${1}"

f_print_help() {
  cat <<EOF
Enable SSL for machine
Usage: bash ${0} <machine_name>
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

if [ -z "${MACHINE_NAME}" ]
then
  f_log_err "No machine name"
  exit 1
fi

f_check_variables "DOMAIN_HOME NODEMANAGER_USERNAME NODEMANAGER_PASSWORD"

f_start

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
from wlst_log import *

log_info('Reading domain ${DOMAIN_HOME}')
readDomain('${DOMAIN_HOME}')

log_info('Enable machine SSL')
cd('/Machine/${MACHINE_NAME}/NodeManager/${MACHINE_NAME}')
set('NMType','SSL')

updateDomain()
closeDomain()

exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Enabling failed"

f_finish