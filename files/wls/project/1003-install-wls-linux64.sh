#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi
f_start

f_print_help() {
  cat <<EOF
Install Oracle WebLogic. Place installer zip file into ${SCRIPT_DIR}/wls directory.  
Usage: bash ${0}
EOF
}

if [ "$1" = "--help" ] || [ "$1" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "ORACLE_HOME JAVA_HOME INSTALL_GROUP"

INSTALLER_DIR="$SCRIPT_DIR/wls"
INSTALLER_TMP_DIR="$INSTALLER_DIR/tmp"
export _JAVA_OPTIONS="-Djava.io.tmpdir=${INSTALLER_TMP_DIR}"

mkdir -p "${ORACLE_HOME}"
f_log_info "Target installation path is ${ORACLE_HOME}"
f_log_info "Target inventory path is ${INVENTORY_PATH}"
f_log_info "Target inventory group is ${INSTALL_GROUP}"

cd "${INSTALLER_DIR}"

WLS_ZIP="$(find "${PWD}" -type f -name "*.zip" | head -1)"
if [ -z "${WLS_ZIP}" ]
then
  f_error_exit 2 "Missing installer zip file"
fi
f_log_info "Installer zip file found. ${WLS_ZIP}"

WLS_JAR="$(zipinfo "${WLS_ZIP}" | grep -P 'fmw_.*_wls.jar$' | awk '{print $NF}')"
if [ -z "$WLS_JAR" ]
then
  f_error_exit 4 "Missing installer jar from zip file"
fi
f_log_info "Installer jar found. ${WLS_JAR}"
f_log_info "Installer version $(echo "${WLS_JAR}" | cut -d'_' -f2)"

f_log_info "Create temporary directory"
mkdir "${INSTALLER_TMP_DIR}"
cd "${INSTALLER_TMP_DIR}"

f_log_info "Extracting ${WLS_JAR} from ${WLS_ZIP}"
unzip -oqq "${WLS_ZIP}" "${WLS_JAR}"
f_error_exit $? "Extraction error"

INSTALL_INVENTORY_FILE="${INSTALLER_TMP_DIR}/inventory.loc.tmp"
INSTALL_RESPONSE_FILE="${INSTALLER_TMP_DIR}/install.resp.tmp"

f_log_info "Creating installer inventory file ${INSTALL_INVENTORY_FILE}"
cat <<EOF > "${INSTALL_INVENTORY_FILE}"
inventory_loc=${INVENTORY_PATH}
inst_group=${INSTALL_GROUP}
EOF

f_log_info "Creating installer response file ${INSTALL_RESPONSE_FILE}"
cat <<EOF > "${INSTALL_RESPONSE_FILE}"
[ENGINE]
Response File Version=1.0.0.0.0

[GENERIC]
DECLINE_AUTO_UPDATES=true
MOS_USERNAME=
MOS_PASSWORD=<SECURE VALUE>
AUTO_UPDATES_LOCATION=
SOFTWARE_UPDATES_PROXY_SERVER=
SOFTWARE_UPDATES_PROXY_PORT=
SOFTWARE_UPDATES_PROXY_USER=
SOFTWARE_UPDATES_PROXY_PASSWORD=<SECURE VALUE>
ORACLE_HOME=${ORACLE_HOME}
FEDERATED_ORACLE_HOMES=
INSTALL_TYPE=WebLogic Server
EOF

f_log_info "Removing current inventory at ${INVENTORY_PATH}"
rm -rf "${INVENTORY_PATH}"

f_log_info "Removing current installation at ${ORACLE_HOME}"
rm -rf "${ORACLE_HOME}"

f_log_info "Installing using response files"
java -jar "${WLS_JAR}" -silent -invPtrLoc "${INSTALL_INVENTORY_FILE}" -responseFile "${INSTALL_RESPONSE_FILE}"
f_error_exit $? "Installation error"

f_log_info "Cleaning up temporary directory"
cd "${INSTALLER_DIR}"
rm -rf "${INSTALLER_TMP_DIR}"

cd "${SCRIPT_DIR}"

f_finish
