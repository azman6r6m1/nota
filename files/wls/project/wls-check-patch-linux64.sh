#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

LOG_POSTFIX="$1"

source "$(dirname "${0}")/common.sh"
if [ ${?} -ne 0 ]
then
  'ERROR!!! Problem while loading common.sh'
  exit 1
fi
f_start

f_print_help() {
  echo <<EOF
1. Export ORACLE_BASE to the desired path. Use same path for all scripts.
2. This script will use JDK from $ORACLE_BASE/jdk and installation home from $ORACLE_BASE/home.
3. This script will use $ORACLE_BASE/tmp as temporary directory.
3. Run this script using Bash (bash $0). Better to run with nohup.
EOF
}

if [ "$1" = "--help" ] || [ "$1" = "-h" ]
then
  f_print_help
  exit 0
fi

if [ -z "${ORACLE_BASE}" ]
then
  f_log_err "ORACLE_BASE is not set"
  exit 2
fi

f_log_info "Starting WebLogic Server SPB patch"
f_log_info "ORACLE_BASE is set to ${ORACLE_BASE}"

export JAVA_HOME="${ORACLE_BASE}/jdk"
export PATH="${JAVA_HOME}/bin:${PATH}"
export _JAVA_OPTIONS="-Djava.io.tmpdir=${PATCH_TMP_DIR}"
f_log_info "JAVA_HOME is set to ${JAVA_HOME}"

export ORACLE_HOME="${ORACLE_BASE}/home"
export PATH="${ORACLE_HOME}/OPatch:${PATH}"
f_log_info "ORACLE_HOME is set to ${ORACLE_HOME}"

f_log_info "Hostname"
hostname

f_log_info "Current user and groups"
id

f_log_info "Operating System"
cat /etc/os-release | grep -P '^PRETTY_NAME='

f_log_info "Kernel"
uname -a

f_log_info "CPU"
lscpu | grep -P '^(Architecture|CPU\(s\)|CPU MHz): '

f_log_info "Memory"
lsmem | grep '^Total online memory: '

f_log_info "Swap"
swapon --show

f_log_info "Disk"
df -Th

f_log_info "Java version"
java -version 2>&1 | grep 'java version '

f_log_info "OPatch version"
opatch version | head -1

f_log_info "Installation"
grep '<distribution status="installed"' "${ORACLE_HOME}/inventory/registry.xml" | awk '{$1=$1; print}'

f_log_info "Applied patches"
opatch lsinventory -oh "${ORACLE_HOME}" -invPtrLoc "${ORACLE_HOME}/oraInst.loc" | grep -P '^Patch '

THIRDPARTY_DIR="${ORACLE_HOME}/oracle_common/modules/thirdparty"

f_log_info "Checking Log4j version in ${THIRDPARTY_DIR}"
for JAR_FILE in $(find "${THIRDPARTY_DIR}" -type f -name "log4j*.jar")
do
  f_log_info "Checking version info of ${JAR_FILE}"
  unzip -cqq "${JAR_FILE}" "META-INF/MANIFEST.MF" | grep 'Version:'
done

f_finish
