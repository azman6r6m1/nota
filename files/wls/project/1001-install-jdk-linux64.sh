#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi
f_start

f_print_help() {
  cat <<EOF
Install Oracle JDK. Place JDK zip file into ${SCRIPT_DIR}/jdk directory.
Usage: bash ${0}
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "JAVA_HOME"

mkdir -p "${JAVA_HOME}"

INSTALLER_DIR="${SCRIPT_DIR}/jdk"
INSTALLER_TMP_DIR="${INSTALLER_DIR}/tmp"

f_log_info "Target installation path is ${JAVA_HOME}"

cd "${INSTALLER_DIR}"

JDK_ZIP="$(find "${INSTALLER_DIR}" -type f -name "*.zip" | head -1)"
if [ -z "${JDK_ZIP}" ]
then
  f_error_exit 2 "Missing JDK zip file"
fi

f_log_info "JDK zip file found. ${JDK_ZIP}"

JDK_GZIP="$(zipinfo "${JDK_ZIP}" | grep -P 'jdk\-.*\.tar\.gz$' | awk '{print $NF}')"
if [ -z "${JDK_GZIP}" ]
then
  f_error_exit 3 "Missing JDK in tar.gz format within the zip file"
fi
f_log_info "JDK tar.gz found. ${JDK_GZIP}"
f_log_info "JDK version $(echo ${JDK_GZIP} | cut -d'-' -f2)"

f_log_info "Creating temporary directory"
mkdir "${INSTALLER_TMP_DIR}"
cd "${INSTALLER_TMP_DIR}"

f_log_info "Extracting ${JDK_GZIP}  from ${JDK_ZIP}"
unzip -oqq "${JDK_ZIP}" "${JDK_GZIP}"
f_error_exit $? "Extraction error"

JDK_DIR="$(tar tzf "${JDK_GZIP}" | head -1 | sed 's/\/$//')"
f_log_info "Original JDK directory name is ${JDK_DIR}"

f_log_info "Extracting all files from ${JDK_GZIP}"
tar xzf "${JDK_GZIP}"
f_error_exit $? "Extraction error"

f_log_info "Move to backup JDK at ${JAVA_HOME}"
f_move_dir "${JAVA_HOME}"

f_log_info "Moving from $PWD/$JDK_DIR to ${JAVA_HOME}"
mv "${JDK_DIR}" "${JAVA_HOME}"

f_log_info "Cleaning up temporary directory"
cd "${INSTALLER_DIR}"
rm -rf "${INSTALLER_TMP_DIR}"

cd "${SCRIPT_DIR}"

f_finish
