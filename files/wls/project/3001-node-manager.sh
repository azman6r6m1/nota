#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

export REQUIRE_TRUST_STORE=1
source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Start, stop, restart and check status of Oracle WebLogic Node Manager.
The following variables must be defined in wls.env.
  1. DOMAIN_HOME : Oracle WebLogic directory
  2. LOGS_DIR    : Base logs directory

Usage: bash ${0} <start|stop|restart|status>
EOF
}

f_nm_status() {
  local _pid_file="${DOMAIN_HOME}/nodemanager/nodemanager.process.id"
  local _properties_file="${DOMAIN_HOME}/nodemanager/nodemanager.properties"
  local _listen_port=$(grep "^ListenPort" "${_properties_file}" | awk -F"=" '{print $2}')
  local _status="SHUTDOWN"

  if [ -f "${_pid_file}" ]
  then
    local _pid=$(cat "${_pid_file}")
    ps -p ${_pid} 1> /dev/null 2>&1
    local _exit_code=$?
    if [ ${_exit_code} -eq 0 ]
    then
       netstat -an | grep "LISTEN" | grep ":$LISTEN_PORT" 1> /dev/null 2>&1
       _exit_code=$?
       if [ ${_exit_code} -eq 0 ]
       then
         _status="RUNNING"
       fi
    fi
  fi
  echo "${_status}"
}

f_nm_start() {
  local _status="$(f_nm_status)"
  if [ "${_status}" = "RUNNING" ]
  then
    f_log_warn "Skip. Node Manager is ${_status}."
  else
    local _log_file="${LOG_DIR}/nodemanager.log"
    nohup "${DOMAIN_HOME}/bin/startNodeManager.sh" 1>> $_log_file 2>&1 &
    
    local _retry_max=50
    local _retry_wait=3
    local _retry_count=0
    
    local _status="$(f_nm_status)"
    while [ "${_status}" != "RUNNING" ] && [ ${_retry_count} -le ${_retry_max} ]
    do
      ((_retry_count=${_retry_count}+1))
      sleep ${_retry_wait}
      f_log_info ''
      
      _status="$(f_nm_status)"
      if [ "${_status}" = "RUNNING" ]
      then
        f_log_info "Node Manager is RUNNING"
        break
      fi
    done
    
    if [ "${_status}" != "RUNNING" ]
    then
      f_log_err "Reached maximum waiting time. Node Manager is NOT RUNNING"
    fi
  fi
}

f_nm_stop() {
  local _status="$(f_nm_status)"
  if [ "${_status}" != "RUNNING" ]
  then
    f_log_warn "Skip. Node Manager is ${_status}."
  else
    "${DOMAIN_HOME}/bin/stopNodeManager.sh"
  fi
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "DOMAIN_NAME LOGS_DIR"

case "${1}" in
    "status" )
      f_nm_status
    ;;
    
    "start" )
      f_start
      f_log_info "Starting Node Manager"
      f_nm_start
      f_finish
    ;;
    
    "stop" )
      f_start
      f_log_info "Stopping Node Manager"
      f_nm_stop
      f_finish
    ;;
    
    "restart" )
      f_start
      f_log_info "Stopping Node Manager"
      f_nm_stop
      f_log_info "Starting Node Manager"
      f_nm_start
      f_finish
    ;;
    
    * )
      echo "ERROR!!! Invalid option"
    ;;
esac
