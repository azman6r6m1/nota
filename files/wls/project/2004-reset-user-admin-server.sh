#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Reset Admin Server credentials
Usage: bash ${0}
EOF
}

f_start

DATA_DIR="${DOMAIN_HOME}/servers/AdminServer/data"
if [ -d "${DATA_DIR}" ]
then
  f_log_info "Backup data directory"
  f_move_dir "${DATA_DIR}"
  rm -rf "${DATA_DIR}"
fi

f_log_info "Resetting Admin Server credential"
source "${DOMAIN_HOME}/bin/setDomainEnv.sh"
java weblogic.security.utils.AdminAccount "${ADMINSERVER_USERNAME}" "${ADMINSERVER_PASSWORD}" "${DOMAIN_HOME}/security"

f_backup_file "${DOMAIN_HOME}/servers/AdminServer/security/boot.properties"
cat <<EOF > "${DOMAIN_HOME}/servers/AdminServer/security/boot.properties"
username=${ADMINSERVER_USERNAME}
password=${ADMINSERVER_PASSWORD}
EOF

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
import os
from weblogic.security import UsernameAndPassword, UserConfigFileManager
from wlst_log import *

log_info('Creating new config and key files')
System.setProperty("weblogic.management.confirmKeyfileCreation", "true");
user_pass = UsernameAndPassword('${ADMINSERVER_USERNAME}', '${ADMINSERVER_PASSWORD}')
UserConfigFileManager.setUsernameAndPassword(user_pass, '${ADMINSERVER_CONF_FILE}.new', '${ADMINSERVER_KEY_FILE}.new', 'weblogic.management')
os.rename('${ADMINSERVER_CONF_FILE}.new', '${ADMINSERVER_CONF_FILE}')
os.rename('${ADMINSERVER_KEY_FILE}.new', '${ADMINSERVER_KEY_FILE}')

exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Reset user failed"

f_finish
