#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Create additional directories for domains.
Usage: bash ${0}
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "DOMAIN_NAME LOGS_DIR DEPLOYMENTS_DIR ADMIN_SCRIPTS_DIR"

f_start

f_log_info "Create additional directories for ${DOMAIN_NAME}"

f_log_info "Creating logs directory"
mkdir -p "${LOGS_DIR}/${DOMAIN_NAME}/AdminServer"
mkdir -p "${LOGS_DIR}/${DOMAIN_NAME}/NodeManager"
mkdir -p "${LOGS_DIR}/${DOMAIN_NAME}/scripts"

f_log_info "Creating deloyments directory"
mkdir -p "${DEPLOYMENTS_DIR}/${DOMAIN_NAME}"

f_log_info "Creating admin scripts directory"
mkdir -p "${ADMIN_SCRIPTS_DIR}/${DOMAIN_NAME}"

f_finish
