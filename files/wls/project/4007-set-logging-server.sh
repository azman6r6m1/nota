
#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

SERVER_NAME="${1}"

export REQUIRE_TRUST_STORE=1
source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Set server logging
Usage: bash ${0} <server_name>
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_noparam_exit "SERVER_NAME" "${SERVER_NAME}"

f_check_variables "DOMAIN_HOME LOGS_DIR LOG_ROTATE_TIME LOG_ROTATE_DURATION LOG_KEEP_COUNT"

f_start

SERVER_DIR="${DOMAIN_HOME}/servers/${SERVER_NAME}"
SERVER_LOG_DIR="${LOGS_DIR}/${DOMAIN_NAME}/${SERVER_NAME}"

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
from wlst_log import *

${WLST_CONNECT}

edit()
startEdit()

log_info('Setting up server log configuration')
cd('/Servers/${SERVER_NAME}/Log/${SERVER_NAME}')
set('FileName','${SERVER_LOG_DIR}/${SERVER_NAME}.log')
set('LogFileRotationDir','${SERVER_LOG_DIR}/archive')
set('RotationType','byTime')
set('RotationTime','${LOG_ROTATE_TIME} ')
set('FileTimeSpan','${LOG_ROTATE_DURATION}')
set('NumberOfFilesLimited','true')
set('FileCount','${LOG_KEEP_COUNT}')
set('RotateLogOnStartup','false')
set('DateFormatPattern','yyyy-MM-dd hh:mm:ss.SSS z')
set('LogFileSeverity','Info')
set('LoggerSeverity','Info')
set('StdoutSeverity','Info')
set('RedirectStderrToServerLogEnabled','true')
set('RedirectStdoutToServerLogEnabled','true')
set('DomainLogBroadcastSeverity','Info')

log_info('Setting up HTTP log configuration')
cd('/Servers/${SERVER_NAME}/WebServer/${SERVER_NAME}/WebServerLog/${SERVER_NAME}')
set('FileName','${SERVER_LOG_DIR}/access.log')
set('LogFileRotationDir','${SERVER_LOG_DIR}/archive')
set('RotationType','byTime')
set('RotationTime','${LOG_ROTATE_TIME} ')
set('FileTimeSpan','${LOG_ROTATE_DURATION}')
set('NumberOfFilesLimited','true')
set('FileCount','${LOG_KEEP_COUNT}')
set('RotateLogOnStartup','false')
set('DateFormatPattern','yyyy-MM-dd hh:mm:ss.SSS z')

log_info('Setting up data source log configuration')
cd('/Servers/${SERVER_NAME}/DataSource/${SERVER_NAME}/DataSourceLogFile/${SERVER_NAME}')
set('FileName','${SERVER_LOG_DIR}/datasource.log')
set('LogFileRotationDir','${SERVER_LOG_DIR}/archive')
set('RotationType','byTime')
set('RotationTime','${LOG_ROTATE_TIME} ')
set('FileTimeSpan','${LOG_ROTATE_DURATION}')
set('NumberOfFilesLimited','true')
set('FileCount','${LOG_KEEP_COUNT}')
set('RotateLogOnStartup','false')
set('DateFormatPattern','yyyy-MM-dd hh:mm:ss.SSS z')

log_info('Setting up disagnostic configuration')
cd('/Servers/${SERVER_NAME}/ServerDiagnosticConfig/${SERVER_NAME}')
set('ImageDir','${SERVER_LOG_DIR}/diagnostic_images')

log_info('Saving changes')
save()
activate()

disconnect()
exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Set logging failed"

f_finish