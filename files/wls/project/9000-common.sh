#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

umask 0027

FILE_TIMESTAMP="$(date '+%Y_%m_%d_%H_%M_%S')"
START_EPOCH_MILI="$(date '+%s%3N')"

SCRIPT_FILE="$(basename "${0}")"
SCRIPT_NAME="$(echo "${SCRIPT_FILE}" | cut -d'.' -f1)"
SCRIPT_DIR="$(readlink -f $(dirname "${0}"))"

if [ -z "${LOG_DIR}" ]
then
  LOG_DIR="${SCRIPT_DIR}/logs"
fi
LOG_FILE="${LOG_DIR}/${SCRIPT_NAME}-${FILE_TIMESTAMP}${LOG_POSTFIX}.log"
LOG_PIPE="${LOG_FILE}.tmp"

f_log() {
  echo "$(date +'%Y-%m-%d %H:%M:%S.%3N %z') $*"
}

f_log_info() {
  f_log "[INFO ] ${*} ..."
}

f_log_warn() {
  f_log "[WARN ] ${*} !"
}

f_log_err() {
  f_log "[ERROR] ${*} !!!"
  f_log "Run \"bash ${SCRIPT_NAME} -h\" to get more help"
}

f_duration() {
  local _start=${1}
  local _finish=${2}
  local _diff=$((${_finish} - ${_start}))
  local _milis=$((${_diff} % 1000))
  local _seconds=$((${_diff} / 1000 % 60))
  local _minutes=$((${_diff} / 1000 / 60 % 60))
  local _hours=$((${_diff} / 1000 / 60 / 60 % 24))
  local _days=$((${_diff} / 1000 / 60 / 60 / 24))
  local _text=

  if [ ${_seconds} -gt 0 ] || [ ${_milis} -gt 0 ]
  then
    _text="${_seconds}.$(printf "%03d" ${_milis})s"
  fi
  if [ ${_minutes} -gt 0 ]
  then
    _text="${_minutes}m ${_text}"
  fi
  if [ ${_hours} -gt 0 ]
  then
    _text="${_hours}h ${_text}"
  fi
  if [ ${_days} -gt 0 ]
  then
    _text="${_days}d ${_text}"
  fi
  echo "${_text}" | awk '{$1=$1};1'
}

f_check_variables() {
  for _var in ${1}
  do
    if [ -z "$(printenv "${_var}")" ]
    then
      f_log_err "Missing value for variable ${_var}"
      exit 2
    fi
  done
}

f_error_exit() {
  local _exit_code=${1}
  local _error_message="${2}"
  if [ ${_exit_code} -ne 0 ]
  then
    f_log_err "${_error_message}"
    f_log_info "EXITING with code ${_exit_code}. ($(f_duration ${START_EPOCH_MILI} $(date +%s%3N)))"
    exit ${_exit_code}
  fi
}

f_noparam_exit() {
  local _name="${1}"
  local _value="${2}"
  if [ -z "${_value}" ]
  then
    f_error_exit 1 "Missing value for ${_name}"
  fi
}

f_compress_dir() {
  local _source_dir="${1}"
  local _backup_file="${_source_dir}.${FILE_TIMESTAMP}.bkp.tar.gz"
  tar -czf "${_backup_file}" -C "${_source_dir}" "$(basename "${_source_dir}")"
}

f_move_dir() {
  local _source_file="${1}"
  local _backup_file="${_source_file}.${FILE_TIMESTAMP}.bkp"
  mv "${_source_file}" "${_backup_file}"
}

f_backup_file() {
  local _source_file="${1}"
  local _backup_file="${_source_file}.${FILE_TIMESTAMP}.bkp"
  cp -p "${_source_file}" "${_backup_file}"
}

f_delete_append() {
  local _file="${1}"
  local _find_keyword="${2}"
  local _replace_keyword="${3}"
  _find_keyword="$(echo "${_find_keyword}" | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g')"
  sed -i "/^${_find_keyword}\$/d" "${_file}"
  echo "${_replace_keyword}" >> "${_file}"
}

f_finish() {
  local _exit_code=0
  if [ "${1}" = "X" ]
  then
    f_log_warn "Ctrl+C pressed or SIGINT received"
    _exit_code=99
  elif [ -n "${1}" ]
  then
    _exit_code=${1}
  fi
  f_log_info "FINISHED ($(f_duration ${START_EPOCH_MILI} $(date +%s%3N)))"
  f_log_info "Log file ${LOG_FILE}"
  rm -f "${LOG_PIPE}"
  exit ${_exit_code}
}

f_start() {
  trap "f_finish X" SIGINT
  rm -f "${LOG_PIPE}" > /dev/null 2>&1
  mkdir -p "${LOG_DIR}" > /dev/null 2>&1
  mkfifo "${LOG_PIPE}"
  tee -a "${LOG_FILE}" < "${LOG_PIPE}" &
  exec > "${LOG_PIPE}"
  
  f_log_info "STARTED"
  f_log_info "Log file ${LOG_FILE}"
  START_EPOCH_MILI="$(date +%s%3N)"

  export JAVA_TOOL_OPTIONS="-Dpython.path=${SCRIPT_DIR} ${JAVA_TOOL_OPTIONS}"

  if [ -n "${REQUIRE_TRUST_STORE}" ] && [ -f "${TRUST_STORE}.pass" ]
  then
     JAVA_TOOL_OPTIONS="-Dweblogic.security.SSL.hostnameVerifier=weblogic.security.utils.SSLWLSWildcardHostnameVerifier -Dweblogic.security.TrustKeyStore=CustomTrust -Dweblogic.security.CustomTrustKeyStoreFileName=${TRUST_STORE} -Dweblogic.security.TrustKeystoreType=${KEYSTORE_TYPE} -Dweblogic.security.CustomTrustKeyStorePassPhrase=${KEYSTORE_PASSWORD} ${JAVA_TOOL_OPTIONS}"
  fi
    
  export WLST_CONNECT="connect('${ADMINSERVER_USERNAME}', '${ADMINSERVER_PASSWORD}', 't3://${ADMINSERVER_HOSTNAME}:${ADMINSERVER_PORT}')"
  if [ -n "$(grep "<administration-port-enabled>true</administration-port-enabled>" "${DOMAIN_HOME}/config/config.xml")" ]
  then
    export WLST_CONNECT="connect('${ADMINSERVER_USERNAME}', '${ADMINSERVER_PASSWORD}', 't3s://${ADMINSERVER_HOSTNAME}:${ADMIN_PORT}')"
  fi
}

if [ "$(whoami)" != "$(ls -l "${0}" | awk '{print $3}')" ]
then
  f_log_err "ERROR!!! Not owner."
  exit 1
fi

source "${SCRIPT_DIR}/0000-install.env"
