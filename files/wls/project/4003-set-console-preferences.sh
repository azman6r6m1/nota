#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

USERNAME="${1}"

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Set console preferences fpr specific user
Usage: bash ${0} <username>
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_noparam_exit "USERNAME" "${USERNAME}"

f_start

f_check_variables "DOMAIN_HOME"

CONSOLE_DIR="${DOMAIN_HOME}/servers/AdminServer/data/console"
PREFERENCE_FILE="${CONSOLE_DIR}/ConsolePreferences.xml"

f_log_info "Setting up preferences"

if [ ! -f "${PREFERENCE_FILE}" ]
then
  cat <<EOF > "${PREFERENCE_FILE}"
<?xml version="1.0" encoding="UTF-8"?>
<portlet-preferences>
</portlet-preferences>
EOF
fi

sed -i -e 's/<portlet-preferences\/>/\n<portlet-preferences>\n<\/portlet-preferences>/' "${PREFERENCE_FILE}"
sed -i -e '/<\/portlet-preferences>/d' -e "/.*GlobalPreferencesPortlet.*${USERNAME}/,/.*<\/portlet-preference>.*/d"  -e "/.*CoreServerServerTablePortlet.*${USERNAME}/,/.*<\/portlet-preference>.*/d" "${PREFERENCE_FILE}"

cat <<EOF >> "${CONSOLE_DIR}/ConsolePreferences.xml"
  <portlet-preference definitionLabel="GlobalPreferencesPortlet" user="${USERNAME}">
    <preference name="rememberlastpath" description="The description" isModifiable="true" isMultivalued="false" value="true"/>
    <preference name="activationtimeout" description="The description" isModifiable="true" isMultivalued="false" value="300"/>
    <preference name="accessibleconsole" description="The description" isModifiable="true" isMultivalued="false" value="true"/>
    <preference name="rememberedpathcount" description="The description" isModifiable="true" isMultivalued="false" value="4"/>
    <preference name="warnusertakinglock" description="The description" isModifiable="true" isMultivalued="false" value="false"/>
    <preference name="optionalfeatures" description="The description" isModifiable="true" isMultivalued="false" value=""/>
    <preference name="confirmationexplicit" description="The description" isModifiable="true" isMultivalued="false" value="true"/>
    <preference name="filebrowserstart" description="The description" isModifiable="true" isMultivalued="false" value="\"/>
    <preference name="warnuserholdslock" description="The description" isModifiable="true" isMultivalued="false" value="false"/>
    <preference name="showadvanced" description="The description" isModifiable="true" isMultivalued="false" value="true"/>
    <preference name="refreshinterval" description="The description" isModifiable="true" isMultivalued="false" value="10"/>
    <preference name="showinlinehelp" description="The description" isModifiable="true" isMultivalued="false" value="true"/>
  </portlet-preference>
  <portlet-preference definitionLabel="CoreServerServerTablePortlet" user="${USERNAME}">
    <preference name="filterByColumn" description="The description" isModifiable="true" isMultivalued="false" value="name"/>
    <preference name="maxrows" description="The description" isModifiable="true" isMultivalued="false" value="All"/>
    <preference name="filterByCriteria" description="The description" isModifiable="true" isMultivalued="false" value=""/>
    <preference name="selectedColumns" description="The description" isModifiable="true" isMultivalued="true">
      <value>name</value>
      <value>serverType</value>
      <value>clusterName</value>
      <value>machineName</value>
      <value>state</value>
      <value>health</value>
      <value>listenAddress</value>
      <value>listenPort</value>
      <value>sslEnabled</value>
      <value>sslListenPort</value>
      <value>administrationPort</value>
      <value>weblogicVersion</value>
      <value>weblogicPluginEnabled</value>
      <value>javaVersion</value>
      <value>osVersion</value>
      <value>transitionActivity</value>
      <value>heapFreeCurrent</value>
      <value>heapSizeCurrent</value>
      <value>processorLoad</value>
    </preference>
    <preference name="rowsperpage" description="The description" isModifiable="true" isMultivalued="false" value="100"/>
  </portlet-preference>
EOF
echo "</portlet-preferences>" >> "${CONSOLE_DIR}/ConsolePreferences.xml"

f_finish