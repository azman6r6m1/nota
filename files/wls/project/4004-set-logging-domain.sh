
#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

export REQUIRE_TRUST_STORE=1
source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Fix domain logging configuration.
Usage: bash ${0} <server_name>
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "DOMAIN_HOME LOGS_DIR LOG_ROTATE_TIME LOG_ROTATE_DURATION LOG_KEEP_COUNT"

f_start

SERVER_LOG_DIR="${LOGS_DIR}/${DOMAIN_NAME}/Domain"

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
from wlst_log import *

${WLST_CONNECT}

edit()
startEdit()

log_info('Setting up server log configuration')
cd('/Log/${DOMAIN_NAME}')
set('FileName','${SERVER_LOG_DIR}/${DOMAIN_NAME}.log')
set('LogFileRotationDir','${SERVER_LOG_DIR}/archive')
set('RotationType','byTime')
set('RotationTime','${LOG_ROTATE_TIME} ')
set('FileTimeSpan','${LOG_ROTATE_DURATION}')
set('NumberOfFilesLimited','true')
set('FileCount','${LOG_KEEP_COUNT}')
set('RotateLogOnStartup','false')
set('DateFormatPattern','yyyy-MM-dd hh:mm:ss.SSS z')
set('LogFileSeverity','Info')
set('LoggerSeverity','Info')
set('StdoutSeverity','Info')
set('RedirectStderrToServerLogEnabled','true')
set('RedirectStdoutToServerLogEnabled','true')
set('DomainLogBroadcastSeverity','Info')

log_info('Saving changes')
save()
activate()

disconnect()
exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Set logging failed"

f_finish