#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Create new Oracle WebLogic domain. Default username for nodemanager is "nodemanager" and for admin server is "weblogic".  
Usage: bash ${0}
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "JAVA_HOME DOMAIN_NAME DOMAIN_HOME MACHINE_NAME NODEMANAGER_HOSTNAME NODEMANAGER_PORT NODEMANAGER_PASSWORD ADMINSERVER_HOSTNAME ADMINSERVER_PORT ADMINSERVER_PASSWORD"

f_start

if [ -d "${DOMAIN_HOME}" ]
then
  f_log_info "Move to backup current domain directory"
  f_move_dir "${DOMAIN_HOME}"
fi

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
from wlst_log import *

TEMPLATE_NAME = 'Basic WebLogic Server Domain'

log_info('Loading template "' + TEMPLATE_NAME + '"')
selectTemplate(TEMPLATE_NAME)
loadTemplates()

log_info('Setting domain and JDK info')
setOption('DomainName', '${DOMAIN_NAME}')
setOption('JavaHome', '${JAVA_HOME}')
setOption('NodeManagerType', 'PerDomainNodeManager')
setOption('ServerStartMode', 'prod')

log_info('Setting node manager password')
cd('/')
create('${DOMAIN_NAME}', 'SecurityConfiguration')
cd('/SecurityConfiguration/${DOMAIN_NAME}')
set('NodeManagerUsername', 'nodemanager')
set('NodeManagerPasswordEncrypted', '${NODEMANAGER_PASSWORD}')
cd('/')
create('${MACHINE_NAME}', 'Machine')
cd('/Machine/${MACHINE_NAME}')
create('${MACHINE_NAME}', 'NodeManager')
cd('NodeManager/${MACHINE_NAME}')
set('ListenAddress', '${NODEMANAGER_HOSTNAME}')
set('ListenPort', int('${NODEMANAGER_PORT}'))
set('NMType', 'Plain')
set('UserName', 'nodemanager')
set('PasswordEncrypted', '${NODEMANAGER_PASSWORD}')

log_info('Setting admin server host, port and password')
cd('/Servers/AdminServer')
set('ListenAddress', '${ADMINSERVER_HOSTNAME}')
set('ListenPort', ${ADMINSERVER_PORT})
cd('/Security/base_domain/User/weblogic')
set('Password', '${ADMINSERVER_PASSWORD}')

log_info('Creating domain')
writeDomain('${DOMAIN_HOME}')
closeTemplate()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Create domain failed"

f_log_info "Creating temporary credential file for AdminServer"
mkdir -p "${DOMAIN_HOME}/servers/AdminServer/security"
cat <<EOF > "${DOMAIN_HOME}/servers/AdminServer/security/boot.properties"
username=weblogic
password=${ADMINSERVER_PASSWORD}
EOF

f_log_info "Enable plain nodemanager socket"
NODEMANAGER_PROPERTIES="${DOMAIN_HOME}/nodemanager/nodemanager.properties"
f_backup_file "${NODEMANAGER_PROPERTIES}"
f_delete_append "${NODEMANAGER_PROPERTIES}" "SecureListener=.*" "SecureListener=false"

f_log_info "Disabling embedded Derby database during WebLogic startup"
f_backup_file "${DOMAIN_HOME}/bin/setDomainEnv.sh"
sed -i ':a;N;$!ba;s/\nWL_HOME=/\nexport DERBY_FLAG=false\n\nWL_HOME=/' "${DOMAIN_HOME}/bin/setDomainEnv.sh"

f_finish
