#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

KEYSTORE_ALIAS="${1}"

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Enable SSL for Node Manager

Usage: bash ${0}
EOF
}

f_noparam_exit "KEYSTORE_ALIAS" "${KEYSTORE_ALIAS}"

f_start

NODEMANAGER_PROPERTIES="${DOMAIN_HOME}/nodemanager/nodemanager.properties"

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
from wlst_log import *

log_info('Reading domain ${DOMAIN_HOME}')
readDomain('${DOMAIN_HOME}')

log_info('Set the keystore properties')
cd('/')
cd('NMProperties')
set('KeyStores','CustomIdentityAndCustomTrust')
set('CustomIdentityKeyStoreFileName','${IDENTITY_STORE}')
set('CustomIdentityPrivateKeyPassPhrase','${KEYSTORE_PASSWORD}')
set('CustomIdentityAlias','${KEYSTORE_ALIAS}')

updateDomain()
closeDomain()

exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Enabling failed"

f_backup_file "${NODEMANAGER_PROPERTIES}"

f_delete_append "${NODEMANAGER_PROPERTIES}" "CipherSuites=.*" "CipherSuites=TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_RSA_WITH_AES_256_GCM_SHA384,TLS_RSA_WITH_AES_128_GCM_SHA256"
f_delete_append "${NODEMANAGER_PROPERTIES}" "SecureListener=.*" "SecureListener=true"
f_delete_append "${NODEMANAGER_PROPERTIES}" "KeyStores=.*" "KeyStores=CustomIdentityAndCustomTrust"
f_delete_append "${NODEMANAGER_PROPERTIES}" "CustomIdentityKeyStoreFileName=.*" "CustomIdentityKeyStoreFileName=${IDENTITY_STORE}"
f_delete_append "${NODEMANAGER_PROPERTIES}" "CustomIdentityKeyStoreType=.*" "CustomIdentityKeyStoreType=${KEYSTORE_TYPE}"
f_delete_append "${NODEMANAGER_PROPERTIES}" "CustomIdentityKeyStorePassPhrase=.*" "CustomIdentityKeyStorePassPhrase=${KEYSTORE_PASSWORD}"
f_delete_append "${NODEMANAGER_PROPERTIES}" "CustomIdentityAlias=.*" "CustomIdentityAlias=${KEYSTORE_ALIAS}"
f_delete_append "${NODEMANAGER_PROPERTIES}" "CustomIdentityPrivateKeyPassPhrase=.*" "CustomIdentityPrivateKeyPassPhrase=${KEYSTORE_PASSWORD}"
f_delete_append "${NODEMANAGER_PROPERTIES}" "CustomTrustKeyStoreFileName=.*" "CustomTrustKeyStoreFileName=${TRUST_STORE}"
f_delete_append "${NODEMANAGER_PROPERTIES}" "CustomTrustKeystoreType=.*" "CustomTrustKeystoreType=${KEYSTORE_TYPE}"

f_finish
