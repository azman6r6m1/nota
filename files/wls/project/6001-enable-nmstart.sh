
#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

SERVER_NAME="${1}"

export REQUIRE_TRUST_STORE=1
source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Enable node manager start for server
Usage: bash ${0} <server_name>
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_noparam_exit "SERVER_NAME" "${SERVER_NAME}"

f_start

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
from wlst_log import *

${WLST_CONNECT}

log_info('Generate node manager startup properties')
nmGenBootStartupProps('${SERVER_NAME}')

disconnect()
exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Enabling failed"

# rm -f "${DOMAIN_HOME}/servers/${SERVER_NAME}/security/boot.properties"

f_finish