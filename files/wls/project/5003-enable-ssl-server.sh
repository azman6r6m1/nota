#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

SERVER_NAME="${1}"
KEYSTORE_ALIAS="${2}"

export REQUIRE_TRUST_STORE=1
source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi

f_print_help() {
  cat <<EOF
Enable SSL for server
Usage: bash ${0} <server_name> <keystore_alias>
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_noparam_exit "SERVER_NAME" "${SERVER_NAME}"
f_noparam_exit "KEYSTORE_ALIAS" "${KEYSTORE_ALIAS}"

f_check_variables "DOMAIN_HOME KEYSTORE_TYPE KEYSTORE_PASSWORD IDENTITY_STORE TRUST_STORE"

f_start

WLST_SCRIPT="${SCRIPT_DIR}/${SCRIPT_NAME}.wlst.tmp"
cat <<EOF > "${WLST_SCRIPT}"
from wlst_log import *

${WLST_CONNECT}

edit()
startEdit()

log_info('Setting up server configuration and keystores')
cd('/Servers/${SERVER_NAME}')
set('KeyStores','CustomIdentityAndCustomTrust')
set('CustomIdentityKeyStoreFileName','${IDENTITY_STORE}')
set('CustomIdentityKeyStoreType','${KEYSTORE_TYPE}')
set('CustomIdentityKeyStorePassPhrase','${KEYSTORE_PASSWORD}')
set('CustomTrustKeyStoreFileName','${TRUST_STORE}')
set('CustomTrustKeyStoreType','${KEYSTORE_TYPE}')
set('CustomTrustKeyStorePassPhrase','${KEYSTORE_PASSWORD}')

log_info('Setting up SSL configuration')
cd('/Servers/${SERVER_NAME}/SSL/${SERVER_NAME}')
set('ListenPort', '${ADMINSERVER_SSL_PORT}')
set('Enabled','true')
set('JSSEEnabled','true')
set('MinimumTLSProtocolVersion','TLSv1.2')
cmo.setCiphersuites(['TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384','TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384','TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256','TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256','TLS_RSA_WITH_AES_256_GCM_SHA384','TLS_RSA_WITH_AES_128_GCM_SHA256'])
set('IdentityAndTrustLocations','KeyStores')
set('ServerPrivateKeyAlias','${KEYSTORE_ALIAS}')
set('ServerPrivateKeyPassPhrase','${KEYSTORE_PASSWORD}')
set('InboundCertificateValidation','BuiltinSSLValidationOnly')
set('OutboundCertificateValidation','BuiltinSSLValidationOnly')
set('SSLRejectionLoggingEnabled','true')
set('SSLv2HelloEnabled','false')
set('TwoWaySSLEnabled','false')
set('UseClientCertForOutbound','false')
set('UseServerCerts','false')
set('HostnameVerificationIgnored','false')
set('HostnameVerifier','weblogic.security.utils.SSLWLSWildcardHostnameVerifier')

log_info('Saving changes')
save()
activate()

disconnect()
exit()
EOF

wlst.sh "${WLST_SCRIPT}"
WLST_STATUS=${PIPESTATUS[0]}
rm -f "${WLST_SCRIPT}"
f_error_exit ${WLST_STATUS} "Enabling failed"

f_log_info "Update AdminServer boot properties"
if [ "${SERVER_NAME}" = "AdminServer" ]
then
  BOOT_FILE="${DOMAIN_HOME}/servers/${SERVER_NAME}/security/boot.properties"
  f_delete_append "${BOOT_FILE}" "TrustKeyStore=.*" "TrustKeyStore=CustomTrust"
  f_delete_append "${BOOT_FILE}" "CustomTrustKeyStoreFileName=.*" "CustomTrustKeyStoreFileName=${TRUST_STORE}"
  f_delete_append "${BOOT_FILE}" "CustomTrustKeyStorePassPhrase=.*" "CustomTrustKeyStorePassPhrase=${KEYSTORE_PASSWORD}"
fi

f_finish