#!/bin/bash

# By Azman Kudus
# Last update on 7 Feb 2022

source "$(dirname "${0}")/9000-common.sh"
if [ ${?} -ne 0 ]
then
  echo "ERROR!!! Problem while loading 9000-common.sh"
  exit 1
fi
f_start

f_print_help() {
  cat <<EOF
Apply hardened Oracle JDK security policy.
Usage: bash ${0}
EOF
}

if [ "${1}" = "--help" ] || [ "${1}" = "-h" ]
then
  f_print_help
  exit 0
fi

f_check_variables "JAVA_HOME"

f_log_info "Hardening Java security configuration in ${JAVA_HOME}"

SECURITY_FILE="${JAVA_HOME}/jre/lib/security/java.security"

if [ ! -f "${SECURITY_FILE}" ]
then
  f_error_exit 2 "Missing java.security file"
fi

f_log_info "Backup current java.security"
f_backup_file "${JAVA_HOME}/jre/lib/security/java.security"

f_log_info "Writing new java.security"
cat <<EOF > "${JAVA_HOME}/jre/lib/security/java.security"
security.provider.1=sun.security.provider.Sun
security.provider.2=sun.security.rsa.SunRsaSign
security.provider.3=sun.security.ec.SunEC
security.provider.4=com.sun.net.ssl.internal.ssl.Provider
security.provider.5=com.sun.crypto.provider.SunJCE
security.provider.6=sun.security.jgss.SunProvider
security.provider.7=com.sun.security.sasl.Provider
security.provider.8=org.jcp.xml.dsig.internal.dom.XMLDSigRI
security.provider.9=sun.security.smartcardio.SunPCSC

securerandom.source=file:/dev/random
securerandom.strongAlgorithms=NativePRNGBlocking:SUN,DRBG:SUN

login.configuration.provider=sun.security.provider.ConfigFile

policy.provider=sun.security.provider.PolicyFile
policy.url.1=file:\${java.home}/lib/security/java.policy
policy.url.2=file:\${user.home}/.java.policy
policy.expandProperties=true
policy.allowSystemProperty=true
policy.ignoreIdentityScope=false

keystore.type=jks
keystore.type.compat=true

package.access=sun.,\\
  com.sun.xml.internal.,\\
  com.sun.imageio.,\\
  com.sun.istack.internal.,\\
  com.sun.jmx.,\\
  com.sun.media.sound.,\\
  com.sun.naming.internal.,\\
  com.sun.proxy.,\\
  com.sun.corba.se.,\\
  com.sun.org.apache.bcel.internal.,\\
  com.sun.org.apache.regexp.internal.,\\
  com.sun.org.apache.xerces.internal.,\\
  com.sun.org.apache.xpath.internal.,\\
  com.sun.org.apache.xalan.internal.extensions.,\\
  com.sun.org.apache.xalan.internal.lib.,\\
  com.sun.org.apache.xalan.internal.res.,\\
  com.sun.org.apache.xalan.internal.templates.,\\
  com.sun.org.apache.xalan.internal.utils.,\\
  com.sun.org.apache.xalan.internal.xslt.,\\
  com.sun.org.apache.xalan.internal.xsltc.cmdline.,\\
  com.sun.org.apache.xalan.internal.xsltc.compiler.,\\
  com.sun.org.apache.xalan.internal.xsltc.trax.,\\
  com.sun.org.apache.xalan.internal.xsltc.util.,\\
  com.sun.org.apache.xml.internal.res.,\\
  com.sun.org.apache.xml.internal.resolver.helpers.,\\
  com.sun.org.apache.xml.internal.resolver.readers.,\\
  com.sun.org.apache.xml.internal.security.,\\
  com.sun.org.apache.xml.internal.serializer.utils.,\\
  com.sun.org.apache.xml.internal.utils.,\\
  com.sun.org.glassfish.,\\
  com.oracle.xmlns.internal.,\\
  com.oracle.webservices.internal.,\\
  oracle.jrockit.jfr.,\\
  org.jcp.xml.dsig.internal.,\\
  jdk.internal.,\\
  jdk.nashorn.internal.,\\
  jdk.nashorn.tools.,\\
  jdk.xml.internal.,\\
  com.sun.activation.registries.,\\
  com.sun.browser.,\\
  com.sun.glass.,\\
  com.sun.javafx.,\\
  com.sun.media.,\\
  com.sun.openpisces.,\\
  com.sun.prism.,\\
  com.sun.scenario.,\\
  com.sun.t2k.,\\
  com.sun.pisces.,\\
  com.sun.webkit.,\\
  jdk.management.resource.internal.

package.definition=sun.,\\
  com.sun.xml.internal.,\\
  com.sun.imageio.,\\
  com.sun.istack.internal.,\\
  com.sun.jmx.,\\
  com.sun.media.sound.,\\
  com.sun.naming.internal.,\\
  com.sun.proxy.,\\
  com.sun.corba.se.,\\
  com.sun.org.apache.bcel.internal.,\\
  com.sun.org.apache.regexp.internal.,\\
  com.sun.org.apache.xerces.internal.,\\
  com.sun.org.apache.xpath.internal.,\\
  com.sun.org.apache.xalan.internal.extensions.,\\
  com.sun.org.apache.xalan.internal.lib.,\\
  com.sun.org.apache.xalan.internal.res.,\\
  com.sun.org.apache.xalan.internal.templates.,\\
  com.sun.org.apache.xalan.internal.utils.,\\
  com.sun.org.apache.xalan.internal.xslt.,\\
  com.sun.org.apache.xalan.internal.xsltc.cmdline.,\\
  com.sun.org.apache.xalan.internal.xsltc.compiler.,\\
  com.sun.org.apache.xalan.internal.xsltc.trax.,\\
  com.sun.org.apache.xalan.internal.xsltc.util.,\\
  com.sun.org.apache.xml.internal.res.,\\
  com.sun.org.apache.xml.internal.resolver.helpers.,\\
  com.sun.org.apache.xml.internal.resolver.readers.,\\
  com.sun.org.apache.xml.internal.security.,\\
  com.sun.org.apache.xml.internal.serializer.utils.,\\
  com.sun.org.apache.xml.internal.utils.,\\
  com.sun.org.glassfish.,\\
  com.oracle.xmlns.internal.,\\
  com.oracle.webservices.internal.,\\
  oracle.jrockit.jfr.,\\
  org.jcp.xml.dsig.internal.,\\
  jdk.internal.,\\
  jdk.nashorn.internal.,\\
  jdk.nashorn.tools.,\\
  jdk.xml.internal.,\\
  com.sun.activation.registries.,\\
  com.sun.browser.,\\
  com.sun.glass.,\\
  com.sun.javafx.,\\
  com.sun.media.,\\
  com.sun.openpisces.,\\
  com.sun.prism.,\\
  com.sun.scenario.,\\
  com.sun.t2k.,\\
  com.sun.pisces.,\\
  com.sun.webkit.,\\
  jdk.management.resource.internal.

security.overridePropertiesFile=true

ssl.KeyManagerFactory.algorithm=PKIX
ssl.TrustManagerFactory.algorithm=PKIX

networkaddress.cache.negative.ttl=10

krb5.kdc.bad.policy = tryLast
sun.security.krb5.disableReferrals=false
sun.security.krb5.maxReferrals=5

jdk.disabled.namedCurves = secp112r1, secp112r2, secp128r1, secp128r2, \\
  secp160k1, secp160r1, secp160r2, secp192k1, secp192r1, secp224k1, \\
  secp224r1, secp256k1, sect113r1, sect113r2, sect131r1, sect131r2, \\
  sect163k1, sect163r1, sect163r2, sect193r1, sect193r2, sect233k1, \\
  sect233r1, sect239k1, sect283k1, sect283r1, sect409k1, sect409r1, \\
  sect571k1, sect571r1, X9.62 c2tnb191v1, X9.62 c2tnb191v2, \\
  X9.62 c2tnb191v3, X9.62 c2tnb239v1, X9.62 c2tnb239v2, X9.62 c2tnb239v3, \\
  X9.62 c2tnb359v1, X9.62 c2tnb431r1, X9.62 prime192v2, X9.62 prime192v3, \\
  X9.62 prime239v1, X9.62 prime239v2, X9.62 prime239v3, brainpoolP256r1, \\
  brainpoolP320r1, brainpoolP384r1, brainpoolP512r1

jdk.security.legacyAlgorithms=

jdk.certpath.disabledAlgorithms=MD2, MD5, SHA1 jdkCA & usage TLSServer, \\
  RSA keySize < 2048, DSA keySize < 2048, EC keySize < 256, \\
  include jdk.disabled.namedCurves

jdk.jar.disabledAlgorithms=MD2, MD5, SHA1, RSA keySize < 2048, DSA keySize < 2048, EC keySize < 256
  include jdk.disabled.namedCurves

jdk.tls.legacyAlgorithms=

jdk.tls.disabledAlgorithms=SSLv3, TLSv1, TLSv1.1, RC4, DES, MD5withRSA, \\
  RSA keySize < 2048, DSA keySize < 2048, EC keySize < 256, \\
  anon, NULL, K_NULL, C_NULL, M_NULL, DH_anon, ECDH_anon, \\
  RC4_128, RC4_40, DES_CBC, DES40_CBC, 3DES_EDE_CBC, \\
  include jdk.disabled.namedCurves

jdk.tls.keyLimits=AES/GCM/NoPadding KeyUpdate 2^37

jdk.xml.dsig.secureValidationPolicy=\\
  disallowAlg http://www.w3.org/TR/1999/REC-xslt-19991116,\\
  disallowAlg http://www.w3.org/2001/04/xmldsig-more
  disallowAlg http://www.w3.org/2001/04/xmldsig-more
  disallowAlg http://www.w3.org/2001/04/xmldsig-more
  maxTransforms 5,\\
  maxReferences 30,\\
  disallowReferenceUriSchemes file http https,\\
  minKeySize RSA 1024,\\
  minKeySize DSA 1024,\\
  minKeySize EC 224,\\
  noDuplicateIds,\\
  noRetrievalMethodLoops

jceks.key.serialFilter = java.lang.Enum;java.security.KeyRep;\\
  java.security.KeyRep\$Type;javax.crypto.spec.SecretKeySpec;!*

jdk.sasl.disabledMechanisms=

jdk.security.caDistrustPolicies=SYMANTEC_TLS

jdk.tls.alpnCharset=ISO_8859_1
EOF

f_finish
