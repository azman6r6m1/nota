# By Azman Kudus
# Last update on 17 Jan 2022

from datetime import datetime
import traceback

ENABLE_TRACEBACK = False

def log(level, message):
  print '%s [%s] %s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S %z'), level, message)


def log_info(message):
  log('INFO ', message)


def log_warn(message):
  log('WARN ', message)


def log_error(message):
  log('ERROR', message)
  traceback.print_exc()

