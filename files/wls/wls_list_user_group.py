# By Azman Kudus
# Last update on 17 Jan 2022

from wls_connect import *
from wls_log import *'*

AUTHENTICATOR_MBEAN_PATH = '/SecurityConfiguration/domain01/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator'

def list_groups():
  log_info('Available groups')
  
  print '    ' + 'Name'.ljust(25) + '   ' + 'Description'
  print '    ' + '----'.ljust(25) + '   ' + '-----------'
  
  cmo = cd(AUTHENTICATOR_MBEAN_PATH)
  group_cursor = cmo.listGroups('*', 0)
  i = 1
  while cmo.haveCurrent(group_cursor):
    group_name = cmo.getCurrentName(group_cursor)
    group_description = cmo.getGroupDescription(group_name)
    if not group_description:
      group_description = ''
    print (str(i) + '. ').ljust(4) + group_name.ljust(25) + '   ' + group_description
    cmo.advance(group_cursor)
    i += 1
  cmo.close(group_cursor)


def list_users():
  log_info('Available users')
  
  print '    ' + 'Name'.ljust(25) + '   ' + 'Description'
  print '    ' + '----'.ljust(25) + '   ' + '-----------'
  
  cmo = cd(AUTHENTICATOR_MBEAN_PATH)
  user_cursor = cmo.listUsers('*',0)
  i = 1
  while cmo.haveCurrent(user_cursor):
    user_name = cmo.getCurrentName(user_cursor)
    user_description = cmo.getUserDescription(user_name)
    if not user_description:
      user_description = ''
    print (str(i) + '. ').ljust(4) + user_name.ljust(25) + '   ' + user_description
    cmo.advance(user_cursor)
    i += 1
  cmo.close(user_cursor)


def list_users_groups():
  log_info('Groups assigned to users')
  
  print '    ' + 'User'.ljust(25) + '   ' + 'Groups'
  print '    ' + '----'.ljust(25) + '   ' + '------'
  
  cmo = cd(AUTHENTICATOR_MBEAN_PATH)
  user_cursor = cmo.listUsers('*',0)
  
  i = 1
  while cmo.haveCurrent(user_cursor):
    user_name = cmo.getCurrentName(user_cursor)
    
    user_group_cursor = cmo.listMemberGroups(user_name)
    user_group_list = []
    while cmo.haveCurrent(user_group_cursor):
      user_group_list.append(cmo.getCurrentName(user_group_cursor))
      cmo.advance(user_group_cursor)
    cmo.close(user_group_cursor)
    
    print (str(i) + '. ').ljust(4) + user_name.ljust(25) + '   ' + ', '.join(user_group_list)
    cmo.advance(user_cursor)
    i += 1
  cmo.close(user_cursor)


if '__name__' == '__main__':
  connect_weblogic()
  list_groups()
  list_users()
  list_users_groups()
