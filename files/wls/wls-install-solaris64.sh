#!/bin/bash

# By Azman Kudus
# Last update on 17 Jan 2022

shopt -s expand_aliases
alias awk=gawk
alias find=gfind
alias grep=ggrep
alias sed=gsed
alias tar=gtar

CURRENT_USER="$(whoami)"
SCRIPT_OWNER="$(ls -l "${0}" | cut -d' ' -f3)"
SCRIPT_DIR="$(readlink -f $(dirname "${0}"))"
SCRIPT_NAME="$(basename "${0}")"

if [ "${CURRENT_USER}" != "${SCRIPT_OWNER}" ]
then
  log_err "You are not script owner. Login as \"${SCRIPT_OWNER}\" and try again."
  exit 1
fi

umask 027

log() {
  echo "$(date +'%Y-%m-%d %H:%M:%S.%3N %z') $*"
}

log_info() {
  log "[INFO ] ${*} ..."
}

log_warn() {
  log "[WARN ] ${*} !"
}

log_err() {
  log "[ERROR] ${*} !!!"
  log "Run \"bash ${SCRIPT_NAME} -h\" to get more help"
}

INSTALLER_DIR="$SCRIPT_DIR/install"
INSTALLER_TMP_DIR="$INSTALLER_DIR/tmp"

print_help() {
  cat <<EOF
1. Place installer zip file (wls-*-V*.zip) that contains installer jar file into "install".
2. Export ORACLE_BASE to the desired path. Use same path for all scripts.
3. This script will install to $ORACLE_BASE/home and $ORACLE_BASE/oraInventory. Backup if required.
4. Run this script using Bash (bash ${SCRIPT_NAME}). Better to run with nohup.
EOF
}

if [ "$1" = "--help" ] || [ "$1" = "-h" ]
then
  print_help
  exit 0
fi

if [ -z "${ORACLE_BASE}" ]
then
  log_err "ORACLE_BASE is not set"
  exit 2
fi

mkdir -p "${ORACLE_BASE}"

log_info "Starting WebLogic Server installation"
log_info "ORACLE_BASE is set to ${ORACLE_BASE}"

export JAVA_HOME="${ORACLE_BASE}/jdk"
export PATH="${JAVA_HOME}/bin:${PATH}"
export _JAVA_OPTIONS="-Djava.io.tmpdir=${INSTALLER_TMP_DIR}"
log_info "JAVA_HOME is set to ${JAVA_HOME}"
log_info "$(java -version 2>&1 | grep 'java version ')"

INSTALL_PATH="${ORACLE_BASE}/home"
log_info "Target installation path is $INSTALL_PATH"

INVENTORY_PATH="${ORACLE_BASE}/oraInventory"
log_info "Target inventory path is ${INVENTORY_PATH}"

INSTALL_GROUP="$(groups $(whoami) | awk '{print $NF}')"
log_info "Target installation group is ${INSTALL_GROUP}"

cd "${INSTALLER_DIR}"

WLS_ZIP="$(find "$PWD" -type f -name "wls-*-V*.zip" | head -1)"
if [ -z "${WLS_ZIP}" ]
then
  log_err "Missing installer zip file"
  exit 3
fi
log_info "Installer zip file found. ${WLS_ZIP}"

WLS_JAR="$(zipinfo "$WLS_ZIP" | grep -P 'fmw_.*_wls.jar$' | awk '{print $NF}')"
if [ -z "$WLS_JAR" ]
then
  log_err "Missing installer jar from zip file"
  exit 4
fi
log_info "Installer jar found. ${WLS_JAR}"
log_info "Installer version $(echo "${WLS_JAR}" | cut -d'_' -f2)"

log_info "Create temporary directory"
mkdir "${INSTALLER_TMP_DIR}"
cd "${INSTALLER_TMP_DIR}"

log_info "Extracting ${WLS_JAR} from ${WLS_ZIP}"
unzip -oqq "${WLS_ZIP}" "${WLS_JAR}"

INSTALL_INVENTORY_FILE="${INSTALLER_TMP_DIR}/inventory.loc"
INSTALL_RESPONSE_FILE="${INSTALLER_TMP_DIR}/install.resp"

log_info "Creating installer inventory file ${INSTALL_INVENTORY_FILE}"
cat <<EOF > "${INSTALL_INVENTORY_FILE}"
inventory_loc=${INVENTORY_PATH}
inst_group=${INSTALL_GROUP}
EOF

log_info "Creating installer response file ${INSTALL_RESPONSE_FILE}"
cat <<EOF > "${INSTALL_RESPONSE_FILE}"
[ENGINE]
Response File Version=1.0.0.0.0

[GENERIC]
DECLINE_AUTO_UPDATES=true
MOS_USERNAME=
MOS_PASSWORD=<SECURE VALUE>
AUTO_UPDATES_LOCATION=
SOFTWARE_UPDATES_PROXY_SERVER=
SOFTWARE_UPDATES_PROXY_PORT=
SOFTWARE_UPDATES_PROXY_USER=
SOFTWARE_UPDATES_PROXY_PASSWORD=<SECURE VALUE>
ORACLE_HOME=${INSTALL_PATH}
FEDERATED_ORACLE_HOMES=
INSTALL_TYPE=WebLogic Server
EOF

log_info "Removing current inventory at ${INVENTORY_PATH}"
rm -rf "${INVENTORY_PATH}"

log_info "Removing current installation at ${INSTALL_PATH}"
rm -rf "${INSTALL_PATH}"

log_info "Installing using response files"
java -jar "${WLS_JAR}" -silent -invPtrLoc "${INSTALL_INVENTORY_FILE}" -responseFile "${INSTALL_RESPONSE_FILE}"

log_info "Cleaning up temporary directory"
cd "${INSTALLER_DIR}"
rm -rf "${INSTALLER_TMP_DIR}"

cd "${SCRIPT_DIR}"
log_info "Finished"
